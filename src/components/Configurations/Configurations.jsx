import React, { useState, useEffect } from 'react';
import "./Configurations.css";
import { useHistory, useLocation } from "react-router-dom";
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Avatar from 'react-avatar';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';

function Configurations() {

    let history = useHistory();
    let location = useLocation();

    useEffect(() => {
        console.log(history, location);
    }, []);

    const [configData, setconfigData] = useState(
        {
            role: ''
        }
    );

    const [roleListDTO, setroleListDTO] = useState([]);

    const [roleListData, setroleListData] = useState(null);

    const getRoleData = (e) => {
        setconfigData({ ...configData, [e.target.name]: e.target.value });
        console.log(configData);

        if (e.target.value === 'INSTITUTE-ADMIN') {
            setroleListDTO(location.state.data.instituteList);
        }
        else if (e.target.value === 'STUDENT') {
            setroleListDTO(location.state.data.studentUserDTOList);
        }
        else if (e.target.value === 'TEACHER') {
            setroleListDTO(location.state.data.teacherList);
        }

    }

    const goToDashboard = () => {
        if (roleListDTO !== null) {
            localStorage.setItem('InstituteId', roleListData.id);
        }
        console.log(roleListDTO, configData);

        localStorage.setItem('Role', configData.role);

        history.push(
            {
                pathname: '/dashboard'
            }
        );
    }

    const getRoleListData = (e, index) => {
        let newArr = [...roleListDTO];
        setroleListData(newArr[index]);
        console.log(e.target.checked, index, newArr[index]);
    }

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"><br /><br />
                        <FormControl style={{ width: "100%" }}>
                            <InputLabel id="demo-simple-select-label">Class</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={configData.role}
                                onChange={getRoleData}
                                inputProps={{
                                    name: "role"
                                }}
                            >
                                {
                                    location.state.data.roleList.map((option, index) => (
                                        <MenuItem key={index} value={option} >
                                            {option}
                                        </MenuItem>
                                    ))}
                            </Select>
                        </FormControl><br /><br /><br /><br />
                    </div>
                </div>
                <div className="row">
                    {
                        roleListDTO.length === 0 ?
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                <h5>No Data Available !!</h5>
                            </div> :
                            roleListDTO.map((data, index) => (
                                <div key={data.id} className="col-xs-12 col-sm-12 col-md-4 col-lg-4"><br /><br />
                                    <div id="box" className="card" style={{ width: "100%", borderRadius: "12px" }}>
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                                                    <FormControlLabel
                                                        value={data}
                                                        control={<Radio
                                                            key={data.id}
                                                            onChange={e => getRoleListData(e, index)}
                                                        />}
                                                    />
                                                </div>
                                                <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                                                    <Avatar name={data.instituteName} size="35" round={true} />
                                                </div>
                                                <div className="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                                    <h5 style={{ marginTop: "4px" }} className="card-title">{data.instituteName}</h5>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                                                <div className="col-xs-12 col-sm-12 col-md-11 col-lg-11">
                                                    {
                                                        configData.role === "INSTITUTE-ADMIN" ?
                                                            <h5>Institute Type : {data.instituteType.name}</h5> : null
                                                    }
                                                    {
                                                        configData.role === "STUDENT" ?
                                                            <div>
                                                                <h5>Student : {data.studentFirstName + data.studentLastName}</h5>
                                                                <h5>Class : {data.className}</h5>
                                                                <h5>Section: {data.sectionName}</h5>
                                                            </div>
                                                            : null
                                                    }
                                                    {
                                                        configData.role === "TEACHER" ?
                                                            <h5>Teacher : {data.firstName + data.lastName}</h5> : null
                                                    }
                                                </div>
                                            </div>

                                        </div>
                                    </div><br />
                                </div>
                            ))
                    }
                </div>
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                        {
                            roleListDTO.length === 0 ?
                                <Button disabled={
                                    configData.role === ''
                                } onClick={goToDashboard} style={{ fontSize: "100%" }} variant="contained" color="primary">Proceed</Button>
                                :
                                <Button disabled={
                                    configData.role === '' ||
                                    roleListData === null
                                } onClick={goToDashboard} style={{ fontSize: "100%" }} variant="contained" color="primary">Proceed</Button>
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Configurations
