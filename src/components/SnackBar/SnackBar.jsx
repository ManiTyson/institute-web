import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function SnackBar(props) {

    const {handleClose, status , message, openStatus} = props;

    return (
        <div>
            <Snackbar anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
            }} open={openStatus} autoHideDuration={3000} onClose={handleClose}>
                {
                    status === true ?
                        <Alert onClose={handleClose} severity="success">
                            {message}
                        </Alert> :
                        <Alert onClose={handleClose} severity="error">
                            {message}
                        </Alert>
                }
            </Snackbar>
        </div>
    )
}

export default SnackBar
