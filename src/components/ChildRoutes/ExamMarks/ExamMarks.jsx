import React, { useState, useEffect } from 'react';
import "./ExamMarks.css";
import Fab from '@material-ui/core/Fab';
import EditIcon from '@material-ui/icons/Edit';
import axios from 'axios';
import { baseUrl } from '../../ServerUrls';
import { useHistory } from "react-router-dom";
import Snackbar from '../../SnackBar/SnackBar';
import Avatar from 'react-avatar';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import DatePicker from "react-datepicker";
import TextField from '@material-ui/core/TextField';
import "react-datepicker/dist/react-datepicker.css";

function ExamMarks() {

    let history = useHistory();

    const [pageNumber, setpageNumber] = useState(0);
    const [limit, setlimit] = useState(100);

    const [classListData, setclassListData] = useState(null);
    const [examNameList, setexamNameList] = useState(null);
    const [examScheduleList, setexamScheduleList] = useState(null);

    const [examMarksData, setexamMarksData] = useState(null);

    const [classData, setclassData] = useState({
        class: '',
        section: '',
        date: new Date()
    });

    const [examData, setexamData] = useState({
        examName: ''
    });

    const [examScheduleData, setexamScheduleData] = useState({
        examScheduleName: ''
    });

    const [classDTO, setclassDTO] = useState(null);
    const [sectionDTO, setsectionDTO] = useState(null);
    const [examDTO, setexamDTO] = useState(null);
    const [examScheduleDTO, setexamScheduleDTO] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: false });

    const [open, setOpen] = React.useState(false);

    let roleStatus = localStorage.getItem('Role')

    console.log(roleStatus, "RoleStatus");

    useEffect(() => {

        if (roleStatus !== 'STUDENT') {

            var classDto = {
                "instituteId": localStorage.getItem('InstituteId'),
                "pageNumber": pageNumber,
                "limit": limit,
                "keyword": null
            }

            axios.post(baseUrl + '/class/list', classDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
                .then(response => {
                    console.log(response);
                    if (response.data.success === true) {
                        setclassListData(response.data.data);
                    }
                    else {
                        setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                        setclassListData(null);
                        handleClick();
                    }
                })
                .catch(error => {
                    console.log(error, error.response, error.message, error.request);
                    setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                    setclassListData(null);
                    handleClick();
                })

            var examDto = {
                "instituteId": localStorage.getItem('InstituteId'),

            }

            axios.post(baseUrl + '/exam/list', examDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
                .then(response => {
                    console.log(response);
                    if (response.data.success === true) {
                        setexamNameList(response.data.data);
                    }
                    else {
                        setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                        setexamNameList(null);
                        handleClick();
                    }
                })
                .catch(error => {
                    console.log(error, error.response, error.message, error.request);
                    setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                    setexamNameList(null);
                    handleClick();
                })
        }
        else {

            history.push(
                {
                    pathname: "/dashboard"
                }
            )

        }

    }, []);

    const getClassData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        if (classData.class !== '') {
            setclassData({ ...classData, class: e.target.value, section: '' });
            setsectionDTO(null);
        }
        else {
            setclassData({ ...classData, class: e.target.value });
        }
        setclassDTO(classListData.data[child.props.id]);
        console.log(classData, classDTO, sectionDTO);
    }

    const getSectionData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        setclassData({ ...classData, section: e.target.value });
        setsectionDTO(classDTO.sections[child.props.id]);
        console.log(classData, classDTO, sectionDTO);
    }

    const getExamData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        setexamData({ ...examData, examName: e.target.value });
        setexamDTO(examNameList[child.props.id]);
        console.log(examData, examDTO);

    }


    const getExamScheduleData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        setexamScheduleData({ ...examScheduleData, examScheduleName: e.target.value });
        setexamScheduleDTO(examScheduleList[child.props.id]);
        console.log(examScheduleData, examScheduleDTO);
    }


    const changeExamMarks = (e, index) => {
        let newData = { ...examMarksData };
        console.log(e.target.value, index, newData);
        newData.examMarkList[index].scoredMarks = e.target.value;
        setexamMarksData(newData);
        console.log(examMarksData);
    }

    const changeExamComments = (e, index) => {
        let newData = { ...examMarksData };
        console.log(e.target.value, index, newData);
        newData.examMarkList[index].comments = e.target.value;
        setexamMarksData(newData);
        console.log(examMarksData);
    }

    const fetchExamScheduleData = () => {

        var examScheduleDto = {
            "classId": classDTO.id,
            "sectionId": sectionDTO.id,
            "examId": examDTO.id
        }

        axios.post(baseUrl + '/examSchedule/list', examScheduleDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setexamScheduleList(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setexamScheduleList(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setexamScheduleList(null);
                handleClick();
            })

    }

    const fetchExamMarksData = () => {

        var examScheduleDto = {
            "instituteId": localStorage.getItem('InstituteId'),
            "classId": classDTO.id,
            "sectionId": sectionDTO.id,
            "examScheduleId": examScheduleDTO.id
        }

        console.log(examScheduleDto);

        axios.post(baseUrl + '/examMark/institute/list', examScheduleDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setexamMarksData(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setexamMarksData(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setexamMarksData(null);
                handleClick();
            })

    }


    const updateExamMarks = () => {

        console.log(examMarksData);

        axios.post(baseUrl + '/examMark', examMarksData, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: true });
                    handleClick();
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                handleClick();
            })

    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
                        <h5 style={{ color: "#B99E01" }}><i style={{ fontSize: "20px" }} className="fa fa-clock-o"></i>&nbsp;&nbsp;ExamMarks<span style={{ color: "black" }}>Dashboard</span></h5><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <FormControl style={{ width: "100%" }}>
                            <InputLabel id="demo-simple-select-label">Class</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={classData.class}
                                onChange={(e, child) => getClassData(e, child)}
                            >
                                {
                                    classListData === null ? null :
                                        classListData.data.map((option, index) => (
                                            <MenuItem id={index} key={option.id} value={option.name} >
                                                {option.name}
                                            </MenuItem>
                                        ))}
                            </Select>
                        </FormControl>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {
                            classData.class === '' ? null :
                                <div>
                                    <FormControl style={{ width: "100%" }}>
                                        <InputLabel id="demo-simple-select-label">Section</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={classData.section}
                                            onChange={(e, child) => getSectionData(e, child)}
                                        >
                                            {
                                                classDTO.sections === null ? null :
                                                    classDTO.sections.map((option, index) => (
                                                        <MenuItem id={index} key={option.id} value={option.name}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                        </Select>
                                    </FormControl>
                                </div>
                        }
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {
                            classData.section === '' ? null :
                                <div>
                                    <FormControl style={{ width: "100%" }}>
                                        <InputLabel id="demo-simple-select-label">Exam List</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={examData.examName}
                                            onChange={(e, child) => getExamData(e, child)}
                                        >
                                            {
                                                examNameList === null ? null :
                                                    examNameList.map((option, index) => (
                                                        <MenuItem id={index} key={option.id} value={option.name} >
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                        </Select>
                                    </FormControl><br /><br /><br /><br />
                                </div>
                        }
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                        <Button disabled={
                            classData.class === '' ||
                            classData.section === ''
                        } onClick={fetchExamScheduleData} style={{ fontSize: "100%" }} variant="contained" color="primary">Fetch</Button>
                    </div>
                </div>


                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {
                            examScheduleList === null ? null :
                                <FormControl style={{ width: "100%" }}>
                                    <InputLabel id="demo-simple-select-label">Exam Schedule</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={examScheduleData.examScheduleName}
                                        onChange={(e, child) => getExamScheduleData(e, child)}
                                    >
                                        {
                                            examScheduleList === null ? null :
                                                examScheduleList.map((option, index) => (
                                                    <MenuItem id={index} key={option.id} value={option.name}>
                                                        {option.name}
                                                    </MenuItem>
                                                ))}
                                    </Select>
                                </FormControl>
                        }
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                        {
                            examScheduleList === null ? null :
                                <Button disabled={
                                    examScheduleData.examScheduleName === ''
                                } onClick={fetchExamMarksData} style={{ fontSize: "100%" }} variant="contained" color="primary">Fetch</Button>
                        }
                    </div>
                </div>


                {
                    examMarksData === null ? null :

                        <div>
                            <div className="row">
                                <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                                <div className="col-xs-12 col-sm-12 col-md-10 col-lg-10"><br /><br /><br /><br />
                                    <table className="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Registration</th>
                                                <th>Scored Marks</th>
                                                <th>Comments</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                examMarksData.examMarkList.map((data, index) => (
                                                    <tr key={data.studentId}>
                                                        <td>
                                                            <Avatar name={data.studentFirstName + ' ' + data.studentLastName} size="35" round={true} />&nbsp;&nbsp;
                                                                                {data.studentFirstName + ' ' + data.studentLastName}
                                                        </td>
                                                        <td>{data.registrationNumber}</td>
                                                        <td>
                                                            <TextField value={data.scoredMarks === null ? '' : data.scoredMarks} onChange={(e) => changeExamMarks(e, index)} style={{ width: "150px" }} id="standard-basic" label="Scored Marks" />
                                                        </td>
                                                        <td>
                                                            <TextField value={data.comments === null ? '' : data.comments} onChange={(e) => changeExamComments(e, index)} style={{ width: "150px" }} id="standard-basic" label="Comments" />
                                                        </td>
                                                    </tr>
                                                ))}
                                        </tbody>
                                    </table>
                                </div>
                                <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                            </div>


                            <div className="row">
                                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center"><br /><br />
                                    <Button onClick={updateExamMarks} style={{ fontSize: "100%" }} variant="contained" color="primary">Update</Button>
                                </div>
                            </div>
                        </div>
                }

            </div>
            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div >
    )
}

export default ExamMarks



