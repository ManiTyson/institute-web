import React, { useState, useEffect } from 'react';
import "./Subject.css";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useHistory, useLocation } from "react-router-dom";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import { baseUrl } from '../../ServerUrls';
import Snackbar from '../../SnackBar/SnackBar';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import { Modal, ModalHeader, ModalFooter, ModalBody } from 'reactstrap';

function Subject() {

    let location = useLocation();
    let history = useHistory();

    const [subjectData, setsubjectData] = useState({
        subjectName: ''
    });

    const [subjectNameList, setsubjectNameList] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: true });

    const [open, setOpen] = React.useState(false);

    const [subject, setsubject] = useState(false);

    const [callUseEffect, setcallUseEffect] = useState(false);

    const [editSubject, seteditSubject] = useState(false);

    const [editSubjectField, seteditSubjectField] = useState('');

    const [pageNumber, setpageNumber] = useState(0);
    const [limit, setlimit] = useState(100);

    useEffect(() => {
        console.log(history, location);

        var subjectDto = {
            "classId": location.state.data.id,
            "pageNumber": pageNumber,
            "limit": limit,
            "keyword": null
        }

        axios.post(baseUrl + '/subject/list', subjectDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setsubjectNameList(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setsubjectNameList(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setsubjectNameList(null);
                handleClick();
            })

    }, [callUseEffect]);

    const getSubjectData = (e) => {
        setsubjectData({ ...subjectData, [e.target.name]: e.target.value });
        console.log(subjectData, subjectNameList);
    }

    const addSubject = () => {
        setsubject(true);
    }

    const editSubjectFieldHandler = (e) => {
        seteditSubjectField({ ...editSubjectField, name: e.target.value });
        console.log(editSubjectField);
    };

    const editSubjectData = (section) => {
        seteditSubjectField(section);
        seteditSubject(true);
    };

    const toggleSubjectModal = () => {
        seteditSubject(!editSubject);
    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const addOrEditsubjectSubmit = (type) => {

        var subjectDto = {
            "name": type === "CREATE" ? subjectData.subjectName : editSubjectField.name,
            "classId": location.state.data.id,
            "id": type === "CREATE" ? null : editSubjectField.id,
            "type": type
        }

        console.log(subjectDto);

        axios.post(baseUrl + '/subject', subjectDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==' } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: true });
                    setcallUseEffect(!callUseEffect);
                    if(type === "EDIT")
                    toggleSubjectModal();
                    setsubjectData({ ...subjectData, subjectName: '' });
                    setsubject(false);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                }
                handleClick();
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                handleClick();
            })
    }

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-5 col-lg-5"></div>
                    <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <h5 style={{ color: "#B99E01" }}><i style={{ fontSize: "25px" }} className="fa fa-language" aria-hidden="true"></i>&nbsp;&nbsp;Subject<span style={{ color: "black" }}>Details</span></h5><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                    <Button onClick={() => history.goBack()} style={{ fontSize: "100%" }} variant="contained" color="secondary">Go Back</Button> 
                    </div>
                    <div style={{ borderRadius: "4px" }} className="col-xs-12 col-sm-12 col-md-4 col-lg-4"><br />
                        <Fab onClick={addSubject} size="small" color="primary" aria-label="add">
                            <AddIcon />
                        </Fab>&nbsp;&nbsp;Add Subject<br /><br />
                        {subject === false ? null :
                            <div>
                                <TextField name="subjectName" value={subjectData.subjectName} onChange={getSubjectData} style={{ width: "60%" }} label="Subject Name" />
                                <Button disabled={
                                    subjectData.subjectName === ''
                                } onClick={() => addOrEditsubjectSubmit("CREATE")} style={{ fontSize: "100%" }} variant="contained" color="secondary">Add</Button>
                            </div>
                        }<br /><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>
                {subjectNameList === null ? (
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center">
                            <h5>No Subjects Available !!</h5>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                    </div>
                ) :
                    (
                        <div className="row">
                            {subjectNameList.data.map((data, index) => (
                                <div key={data.id} className="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center"><br /><br />
                                    <div id="box" className="card" style={{ width: "100%", borderRadius: "12px" }}>
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                    {data.name}
                                                </div>
                                                <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                    <Fab onClick={() => editSubjectData(data)} color="secondary" size="small" aria-label="edit">
                                                        <EditIcon />
                                                    </Fab>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                    )
                }
            </div>

            {/* ModalSection */}
            <Modal style={{ marginTop: "200px" }} className="my-modal" isOpen={editSubject} toggle={toggleSubjectModal}>
                <ModalHeader cssModule={{ 'modal-title': 'w-100 text-center' }} toggle={toggleSubjectModal}>
                    <Button style={{ fontSize: "60%" }} variant="contained" color="primary">Subject</Button>
                </ModalHeader>
                <ModalBody>
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                        <div className="col-xs-12 col-sm-12 col-md-7 col-lg-7 text-center">
                            <TextField value={editSubjectField.name} onChange={editSubjectFieldHandler} style={{ width: "60%" }} label="Section Name" />
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-center">
                            <Button disabled={editSubjectField.name === ''} onClick={() => addOrEditsubjectSubmit("EDIT")} style={{ fontSize: "90%" }} variant="contained" color="primary">Edit</Button>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button style={{ fontSize: "70%" }} variant="contained" color="secondary" onClick={toggleSubjectModal}>Cancel</Button>
                </ModalFooter>
            </Modal>

            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div>
    )
}

export default Subject


