import React, { useState, useEffect } from 'react';
import "./StudentAttendance.css";
import axios from 'axios';
import { baseUrl } from '../../../ServerUrls';
import { useHistory, useLocation } from "react-router-dom";
import Snackbar from '../../../SnackBar/SnackBar';
import Avatar from 'react-avatar';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Button from '@material-ui/core/Button';

function StudentAttendance() {

    let history = useHistory();
    let location = useLocation();

    const [attendanceDate, setattendanceDate] = useState(new Date());
    const [studentAttendanceData, setstudentAttendanceData] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: false });

    const [open, setOpen] = React.useState(false);

    const days = ["Sun", "Mon", "Tues", "Wed", "Thurs", "Fri", "Sat"];

    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];

    useEffect(() => {

        console.log(history, location);
        fetchAttendance();

    }, []);

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const fetchAttendance = () => {

        var studentAttendanceDto = {
            "instituteId": localStorage.getItem('InstituteId'),
            "classId": location.state.data.classDTO.id,
            "sectionId": location.state.data.sectionDTO.id,
            "studentId": location.state.data.id,
            "month": attendanceDate
        }

        axios.post(baseUrl + '/attendanceTrackerDetail/student/attendance', studentAttendanceDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setstudentAttendanceData(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setstudentAttendanceData(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setstudentAttendanceData(null);
                handleClick();
            })

    }

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
                        <h5 style={{ color: "#B99E01" }}><i className="fa fa-clock-o"></i>&nbsp;&nbsp;Student<span style={{ color: "black" }}>Attendance</span></h5><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                        <DatePicker
                            selected={attendanceDate}
                            onChange={date => setattendanceDate(date)}
                            dateFormat="yyyy/MM/dd"
                            peekNextMonth
                            showMonthDropdown
                            showYearDropdown
                            dropdownMode="select"
                        /><br/><br/>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                    <Button onClick={fetchAttendance} style={{ fontSize: "100%" }} variant="contained" color="primary">Fetch</Button>
                    </div>
                </div>
                {studentAttendanceData === null ? (
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center">
                            <h5>No Attendance Available !!</h5>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                    </div>
                ) :
                    (
                        <div>
                            <div className="row">
                                <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                    Name : {location.state.data.firstName + " " + location.state.data.lastName}<br /><br />
                                </div>
                                <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                    Total Present Days : {studentAttendanceData.totalAbsentDays}<br /><br />
                                </div>
                                <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                    Total Absent Days : {studentAttendanceData.totalPresentDays}<br /><br />
                                </div>
                                <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                    Total Working Days : {studentAttendanceData.totalWorkingDays}<br /><br />
                                </div>
                            </div>
                            <div className="row">
                                {studentAttendanceData.attendanceList.map((data, index) => (
                                    <div key={index} className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                        <div id="box" className="card" style={{ width: "100%", borderRadius: "12px" }}>
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <Avatar name={days[new Date(data.date).getDay()]} size="35" round={true} />&nbsp;
                                                    {days[new Date(data.date).getDay()]}&nbsp;&nbsp;&nbsp;&nbsp;
                                                    {
                                                            new Date(data.date).getDate() + "st" + " " +
                                                            months[new Date(data.date).getMonth()] + " " +
                                                            new Date(data.date).getFullYear()
                                                        }
                                                        <p>Status : {data.status}<br />
                                                       Comments : {data.comments}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><br />
                                    </div>
                                ))}
                            </div>
                        </div>
                    )
                }
            </div>
            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div>
    )
}

export default StudentAttendance
