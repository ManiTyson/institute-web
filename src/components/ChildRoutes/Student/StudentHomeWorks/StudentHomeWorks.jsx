import React, { useState, useEffect } from 'react';
import "./StudentHomeWorks.css";
import axios from 'axios';
import { baseUrl } from '../../../ServerUrls';
import { useHistory, useLocation } from "react-router-dom";
import Snackbar from '../../../SnackBar/SnackBar';
import Avatar from 'react-avatar';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Button from '@material-ui/core/Button';

function StudentHomeWorks() {

    let history = useHistory();
    let location = useLocation();

    const [homeWorkDate, sethomeWorkDate] = useState(new Date());
    const [studentHomeWorkData, setstudentHomeWorkData] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: false });

    const [open, setOpen] = React.useState(false);

    useEffect(() => {

        console.log(history, location);
        fetchHomeWork();

    }, []);

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const viewMediaFiles = (data) => {
        window.open(data.value);
    }

    const fetchHomeWork = () => {

        var studentHomeWorkDto = {
            "classId": location.state.data.classDTO.id,
            "sectionId": location.state.data.sectionDTO.id,
            "studentId": location.state.data.id,
            "date": homeWorkDate
        }

        axios.post(baseUrl + '/homeWork/student/list', studentHomeWorkDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setstudentHomeWorkData(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setstudentHomeWorkData(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setstudentHomeWorkData(null);
                handleClick();
            })

    }

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
                        <h5 style={{ color: "#B99E01" }}><i className="fa fa-clock-o"></i>&nbsp;&nbsp;Student<span style={{ color: "black" }}>Attendance</span></h5><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                        <DatePicker
                            selected={homeWorkDate}
                            onChange={date => sethomeWorkDate(date)}
                            dateFormat="yyyy/MM/dd"
                            peekNextMonth
                            showMonthDropdown
                            showYearDropdown
                            dropdownMode="select"
                        /><br /><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                        <Button onClick={fetchHomeWork} style={{ fontSize: "100%" }} variant="contained" color="primary">Fetch</Button>
                    </div>
                </div>
                {studentHomeWorkData === null ? (
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center">
                            <h5>No HomeWork Available !!</h5>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                    </div>
                ) :
                    (
                        <div>
                            {
                                studentHomeWorkData.homeWorkList.length === 0 ? null :
                                    <div className="row">
                                        {
                                            studentHomeWorkData.homeWorkList.map((homeData, index) => (
                                                <div key={index} className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    <div id="box" className="card" style={{ width: "100%", borderRadius: "12px" }}>
                                                        <div className="card-body">
                                                            <div className="row">
                                                                <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                                                                    <Avatar name={homeData.subjectName} size="35" round={true} />
                                                                </div>
                                                                <div className="col-xs-12 col-sm-12 col-md-11 col-lg-11">
                                                                    <h5 style={{ marginTop: "4px" }} className="card-title">{homeData.subjectName}</h5>
                                                                </div>
                                                            </div>
                                                            <div className="row">
                                                                <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                                                                <div className="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                                                    <h5>HW Status -
                                                                        {
                                                                            homeData.isHomeWorkUpdated === true ? 
                                                                            <span> Updated&nbsp;<i style={{color:"green"}} className="fa fa-check" aria-hidden="true"></i></span> : 
                                                                            <span> NotUpdated&nbsp;<i style={{color:"red"}} className="fa fa-exclamation" aria-hidden="true"></i></span>
                                                                        }
                                                                    </h5>
                                                                    {
                                                                        homeData.isHomeWorkUpdated === false ? null :
                                                                            <h5>Description - {homeData.description}</h5>
                                                                    }
                                                                    {
                                                                        homeData.contents === null ? null :
                                                                            <table className="table table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>Sl.No</th>
                                                                                        <th>Name</th>
                                                                                        <th>Type</th>
                                                                                        <th>View</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    {
                                                                                        homeData.contents.map((data, index) => (
                                                                                            <tr key={data.priority}>
                                                                                                <td>{data.priority}</td>
                                                                                                <td>{data.name}</td>
                                                                                                <td>{data.type}</td>
                                                                                                <td>
                                                                                                    <Button onClick={() => viewMediaFiles(data)} style={{ fontSize: "70%" }} variant="contained" color="secondary">View</Button>
                                                                                                </td>
                                                                                            </tr>
                                                                                        ))}
                                                                                </tbody>
                                                                            </table>
                                                                    }
                                                                </div>
                                                                <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                                                            </div>
                                                        </div>
                                                    </div><br />
                                                </div>
                                            ))}
                                    </div>
                            }
                        </div>
                    )
                }
            </div>
            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div>
    )
}

export default StudentHomeWorks

