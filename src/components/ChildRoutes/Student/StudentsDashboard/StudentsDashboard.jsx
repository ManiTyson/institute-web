import React, { useState, useEffect } from 'react';
import "./StudentsDashboard.css";
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import axios from 'axios';
import { baseUrl } from '../../../ServerUrls';
import { useHistory, useLocation } from "react-router-dom";
import Snackbar from '../../../SnackBar/SnackBar';
import Avatar from 'react-avatar';
import StepProgress from '../../../StepProgress/StepProgress';
import Button from '@material-ui/core/Button';

function StudentsDashboard() {

    let history = useHistory();
    let location = useLocation();

    const [pageNumber, setpageNumber] = useState(0);
    const [limit, setlimit] = useState(100);
    const [studentsData, setstudentsData] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: false });

    const [open, setOpen] = React.useState(false);

    let roleStatus = localStorage.getItem('Role')

    console.log(roleStatus, "RoleStatus");

    useEffect(() => {

        console.log(history, location);

        var studentDto = {
            "instituteId": localStorage.getItem('InstituteId'),
            "pageNumber": pageNumber,
            "limit": limit,
            "keyword": null
        }

        axios.post(baseUrl + '/student/list', studentDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setstudentsData(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setstudentsData(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setstudentsData(null);
                handleClick();
            })

    }, []);


    const addOrEditStudent = (status, data) => {

        if (data !== null) {
            data.classDTO.type = "EDIT";
            data.sectionDTO.type = "EDIT";
            for (var i = 0; i < data.classDTO.sections.length; i++) {
                data.classDTO.sections[i].type = "EDIT";
            }
        }

        history.push(
            {
                pathname: "/dashboard/institute/student/add",
                state: {
                    studentType: status,
                    data: data
                }
            }
        )
    }

    const openStudentAttendance = (data) => {

        history.push(
            {
                pathname: "/dashboard/student/attendance",
                state: {
                    data: data
                }
            }
        )

    }

    const openStudentMarks = (data) => {

        history.push(
            {
                pathname: "/dashboard/student/marks",
                state: {
                    data: data
                }
            }
        )

    }

    const openStudentHomeWorks = (data) => {

        history.push(
            {
                pathname: "/dashboard/student/homeWorks",
                state: {
                    data: data
                }
            }
        )

    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    return (
        <div>
            <StepProgress /><br /><br />
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
                        <h5 style={{ color: "#B99E01" }}><i className="fas fa-user-graduate"></i>&nbsp;&nbsp;Students<span style={{ color: "black" }}>Dashboard</span></h5><br />
                        {
                            roleStatus === 'INSTITUTE-ADMIN' ?
                                <div>
                                    <Fab onClick={() => addOrEditStudent('CREATE', null)} size="small" color="primary" aria-label="add">
                                        <AddIcon />
                                    </Fab>&nbsp;&nbsp;Add students<br /><br /><br /><br />
                                </div>
                                : null
                        }
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>
                {studentsData === null ? (
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center">
                            <h5>No students Available !!</h5>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                    </div>
                ) :
                    (
                        <div className="row">
                            {studentsData.data.map((data, index) => (
                                <div key={data.id} className="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                    <div id="box" className="card" style={{ width: "100%", borderRadius: "12px" }}>
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                                                    <Avatar name={data.firstName + ' ' + data.lastName} size="35" round={true} />
                                                </div>
                                                <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                    <h5 style={{ marginTop: "4px" }} className="card-title">{data.firstName + ' ' + data.lastName}</h5>
                                                </div>
                                                <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                    {
                                                        roleStatus === 'INSTITUTE-ADMIN' ?
                                                            <div>
                                                                <Fab onClick={() => addOrEditStudent('EDIT', data)} color="secondary" size="small" aria-label="edit">
                                                                    <EditIcon />
                                                                </Fab>
                                                            </div>
                                                            : null
                                                    }
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                                                <div className="col-xs-12 col-sm-12 col-md-11 col-lg-11">
                                                    <h5>Reg No : {data.registrationNumber}</h5>
                                                    <h5>Mobile Number : {data.mobileNumber}</h5>
                                                    <h5>Class Name : {data.classDTO.name}</h5>
                                                    <h5>Section Name : {data.sectionDTO.name}</h5>
                                                    <h5>Address : {data.address}</h5>
                                                    <h5>Gender : {data.gender}</h5>
                                                    <h5>Date Of Birth : {data.dateOfBirth}</h5>
                                                    <h5>Father Name : {data.fatherName}</h5>
                                                    <h5>Mother Name : {data.motherName}</h5>
                                                    <h5>Emergency Contact Name : {data.emergencyContactName}</h5>
                                                    <h5>Emergency Contact No : {data.emergencyContact}</h5>
                                                    <Button onClick={() => openStudentAttendance(data)} style={{ fontSize: "100%" }} variant="contained" color="primary">Attendance</Button>&nbsp;&nbsp;
                                                    <Button onClick={() => openStudentMarks(data)} style={{ fontSize: "100%" }} variant="contained" color="primary">Marks</Button>&nbsp;&nbsp;
                                                    <Button onClick={() => openStudentHomeWorks(data)} style={{ fontSize: "100%" }} variant="contained" color="primary">HomeWork</Button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><br />
                                </div>
                            ))}
                        </div>
                    )
                }
            </div>
            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div>
    )
}

export default StudentsDashboard

