import React, { lazy, Suspense } from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import LazyLoader from '../LazyLoader/LazyLoader';
import OnlineResource from './OnlineResource/OnlineResource';

function ChildRoutes(props) {

    const InstituteHome = lazy(() => import('./Institute/Home/Home'));
    const InstituteOnboard = lazy(() => import('./Institute/Onboard/Onboard'));
    const StudentHome = lazy(() => import('./Student/Home/Home'));
    const StudentsDashboard = lazy(() => import('./Student/StudentsDashboard/StudentsDashboard'));
    const AddStudents = lazy(() => import('./Student/AddStudents/AddStudents'));
    const TeacherHome = lazy(() => import('./Teacher/Home/Home'));
    const TeachersDashboard = lazy(() => import('./Teacher/TeachersDashboard/TeachersDashboard'));
    const AddTeachers = lazy(() => import('./Teacher/AddTeachers/AddTeachers'));
    const MetaDataServices = lazy(() => import('../Services/MetaServices/MetaServices'));
    const ClassAndSectionDashboard = lazy(() => import('./ClassAndSection/ClassAndSectionDashboard/ClassAndSectionDashboard'));
    const AddClassAndSection = lazy(() => import('./ClassAndSection/AddClassAndSection/AddClassAndSection'));
    const TimetableDashboard = lazy(() => import('./Timetable/TimetableDashboard/TimetableDashboard'));
    const AddTimetableDashboard = lazy(() => import('./Timetable/AddTimetable/AddTimetable'));
    const Subject = lazy(() => import('./Subject/Subject'));
    const Attendance = lazy(() => import('./Attendance/Attendance'));
    const StudentAttendance = lazy(() => import('./Student/StudentAttendance/StudentAttendance'));
    const ExamDashboard= lazy(() => import('./Exam/ExamDashboard/ExamDashboard'));
    const ExamScheduleDashboard = lazy(() => import('./Exam/ExamScheduleDashboard/ExamScheduleDashboard'));
    const AddExamScheduleDashboard = lazy(() => import('./Exam/AddExamScheduleDashboard/AddExamScheduleDashboard'));
    const ExamMarks = lazy(() => import('./ExamMarks/ExamMarks'));
    const StudentMarks = lazy(() => import('./Student/StudentMarks/StudentMarks'));
    const StudentHomeWorks = lazy(() => import('./Student/StudentHomeWorks/StudentHomeWorks'));
    const SyllabusTrackerDashboard = lazy(() => import('./SyllabusTracker/SyllabusTrackerDashboard/SyllabusTrackerDashboard'));
    const AddSyllabusTrackerContent = lazy(() => import('./SyllabusTracker/AddSyllabusTrackerContent/AddSyllabusTrackerContent'));
    const HomeWorkDashboard = lazy(() => import('./HomeWork/HomeWorkDashboard/HomeWorkDashboard'));
    const OnlineResource = lazy(() => import('./OnlineResource/OnlineResource'));

    return (
        <div>
            <Suspense fallback={LazyLoader()}>
                <Switch>
                    <Route exact path="/dashboard">
                        <Redirect to="/dashboard/institute/home" />
                    </Route>
                    <Route exact path="/dashboard/institute/home">
                        <InstituteHome />
                    </Route>
                    <Route exact path="/dashboard/institute/onboard">
                        <MetaDataServices>
                            {(metaData) => (
                                <InstituteOnboard metaData={metaData} />
                            )}
                        </MetaDataServices>
                    </Route>
                    <Route exact path="/dashboard/institute/student">
                        <StudentsDashboard />
                    </Route>
                    <Route exact path="/dashboard/institute/student/add">
                        <AddStudents />
                    </Route>
                    <Route exact path="/dashboard/student/home">
                        <StudentHome />
                    </Route>
                    <Route exact path="/dashboard/student/studentsDashboard">
                        <StudentsDashboard />
                    </Route>
                    <Route exact path="/dashboard/institute/teacher">
                        <TeachersDashboard />
                    </Route>
                    <Route exact path="/dashboard/institute/teacher/add">
                        <AddTeachers />
                    </Route>
                    <Route exact path="/dashboard/teacher/home">
                        <TeacherHome />
                    </Route>
                    <Route exact path="/dashboard/teacher/teachersDashboard">
                        <TeachersDashboard />
                    </Route>
                    <Route exact path="/dashboard/institute/class">
                        <ClassAndSectionDashboard />
                    </Route>
                    <Route exact path="/dashboard/institute/class/add">
                        <AddClassAndSection />
                    </Route>
                    <Route exact path="/dashboard/institute/timetable">
                        <TimetableDashboard />
                    </Route>
                    <Route exact path="/dashboard/institute/timetable/add">
                        <AddTimetableDashboard />
                    </Route>
                    <Route exact path="/dashboard/institute/subject/add">
                        <Subject />
                    </Route>
                    <Route exact path="/dashboard/attendance">
                        <Attendance />
                    </Route>
                    <Route exact path="/dashboard/student/attendance">
                        <StudentAttendance />
                    </Route>
                    <Route exact path="/dashboard/examDashboard">
                        <ExamDashboard />
                    </Route>
                    <Route exact path="/dashboard/examScheduleDashboard">
                        <ExamScheduleDashboard />
                    </Route>
                    <Route exact path="/dashboard/examSchedule/add">
                        <AddExamScheduleDashboard />
                    </Route>
                    <Route exact path="/dashboard/examMarks">
                        <ExamMarks />
                    </Route>
                    <Route exact path="/dashboard/student/marks">
                        <StudentMarks />
                    </Route>
                    <Route exact path="/dashboard/student/homeWorks">
                        <StudentHomeWorks />
                    </Route>
                    <Route exact path="/dashboard/syllabusTrackerContent/add">
                        <AddSyllabusTrackerContent />
                    </Route>
                    <Route exact path="/dashboard/syllabusTrackerDashboard">
                        <SyllabusTrackerDashboard />
                    </Route>
                    <Route exact path="/dashboard/homeWorkDashboard">
                        <HomeWorkDashboard />
                    </Route>
                    <Route exact path="/dashboard/onlineResource">
                        <OnlineResource />
                    </Route>
                    <Route>
                        {props.children}
                    </Route>
                </Switch>
            </Suspense>
        </div>
    )
}

export default ChildRoutes
