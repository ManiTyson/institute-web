import React, { useState, useEffect } from 'react';
import "./OnlineResource.css";
import axios from 'axios';
import { baseUrl } from '../../ServerUrls';
import { useHistory, useLocation } from "react-router-dom";
import Snackbar from '../../SnackBar/SnackBar';
import Avatar from 'react-avatar';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import $ from 'jquery';

function OnlineResource() {

    let history = useHistory();
    let location = useLocation();

    const boardType = [
        {
            "id": "1",
            "name": "STATE-BOARD"
        },
        {
            "id": "2",
            "name": "CBSC"
        },
        {
            "id": "3",
            "name": "MATRICULATION"
        }
    ];

    const contentType = [
        {
            "id": "1",
            "name": "E_BOOK"
        },
        {
            "id": "2",
            "name": "PREVIOUS_YEAR_QUESTION_PAPER"
        },
        {
            "id": "3",
            "name": "EXAM_PRESENTATION"
        }
    ];

    const [onlineResourceDate, setonlineResourceDate] = useState(new Date());

    const [onlineResourceListData, setonlineResourceListData] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: false });

    const [open, setOpen] = React.useState(false);

    const [classListData, setclassListData] = useState(null);

    const [onlineResourceData, setonlineResourceData] = useState({
        class: '',
        subject: '',
        board: '',
        content: '',
        exam: ''
    });

    const [subjectNameList, setsubjectNameList] = useState(null);

    const [examNameList, setexamNameList] = useState(null);

    const [subjectDTO, setsubjectDTO] = useState(null);

    const [classDTO, setclassDTO] = useState(null);

    const [boardDTO, setboardDTO] = useState(null);

    const [contentDTO, setcontentDTO] = useState(null);

    const [examDTO, setexamDTO] = useState(null);

    const [pageNumber, setpageNumber] = useState(0);
    const [limit, setlimit] = useState(100);

    const [fileData, setfileData] = useState(null);

    $(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    let roleStatus = localStorage.getItem('Role')

    console.log(roleStatus, "RoleStatus");

    useEffect(() => {

        console.log(history, location);

        if (roleStatus !== 'TEACHER') {

            var commonDto = {
                "instituteId": "COMMON"
            }

            axios.post(baseUrl + '/class/list', commonDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
                .then(response => {
                    console.log(response);
                    if (response.data.success === true) {
                        setclassListData(response.data.data);
                    }
                    else {
                        setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                        setclassListData(null);
                        handleClick();
                    }
                })
                .catch(error => {
                    console.log(error, error.response, error.message, error.request);
                    setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                    setclassListData(null);
                    handleClick();
                })

            axios.post(baseUrl + '/exam/list', commonDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
                .then(response => {
                    console.log(response);
                    if (response.data.success === true) {
                        setexamNameList(response.data.data);
                    }
                    else {
                        setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                        setexamNameList(null);
                        handleClick();
                    }
                })
                .catch(error => {
                    console.log(error, error.response, error.message, error.request);
                    setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                    setexamNameList(null);
                    handleClick();
                })

        }
        else {

            history.push(
                {
                    pathname: "/dashboard"
                }
            )

        }

    }, []);

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const getContentData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        setonlineResourceData({ ...onlineResourceData, content: e.target.value });
        setcontentDTO(contentType[child.props.id]);
        console.log(contentDTO);
    }

    const getBoardData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        setonlineResourceData({ ...onlineResourceData, board: e.target.value });
        setboardDTO(boardType[child.props.id]);
        console.log(boardDTO);
    }

    const getSubjectData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        setonlineResourceData({ ...onlineResourceData, subject: e.target.value });
        setsubjectDTO(subjectNameList.data[child.props.id]);
        console.log(subjectDTO);
    }

    const getExamData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        setonlineResourceData({ ...onlineResourceData, exam: e.target.value });
        setexamDTO(examNameList[child.props.id]);
        console.log(examDTO);

    }

    const getClassData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        if (onlineResourceData.class !== '') {
            setonlineResourceData({ ...onlineResourceData, class: e.target.value, subject: '' });
            setsubjectDTO(null);
        }
        else {
            setonlineResourceData({ ...onlineResourceData, class: e.target.value });
        }
        setclassDTO(classListData.data[child.props.id]);
        console.log(onlineResourceData, classDTO, subjectDTO);

        var subjectDto = {
            "classId": classListData.data[child.props.id].id,
            "pageNumber": pageNumber,
            "limit": limit,
            "keyword": null
        }

        console.log(subjectDto, "SubjectDto");

        axios.post(baseUrl + '/subject/list', subjectDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setsubjectNameList(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setsubjectNameList(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setsubjectNameList(null);
                handleClick();
            })

    }

    const [editState, seteditState] = useState({
        status: false
    });

    const uploadFile = (e) => {

        console.log(e.target.files.length);
        if (e.target.files.length === 1) {
            console.log(e.target.files, "cool", e.target.files[0].name);
            setfileData(e.target.files[0]);
            console.log(fileData);
        }
        else {
            setfileData(null);
            document.getElementById('customFile').value = "";
            var fileName = "Choose file";
            $(".custom-file-input").siblings(".custom-file-label").addClass("selected").html(fileName);
        }

    }

    const viewMediaFiles = (data) => {
        window.open(data.fileUrl);
    }

    const editMediaFiles = () => {
        seteditState({ ...editState, status: true });
        setfileData(null);
        document.getElementById('customFile').value = "";
        var fileName = "Choose file";
        $(".custom-file-input").siblings(".custom-file-label").addClass("selected").html(fileName);
    }

    const removeEditMode = () => {
        seteditState({ ...editState, status: false });
        setfileData(null);
        document.getElementById('customFile').value = "";
        var fileName = "Choose file";
        $(".custom-file-input").siblings(".custom-file-label").addClass("selected").html(fileName);
    }

    const fetchOnlineResource = () => {

        seteditState({ ...editState, status: false });
        setfileData(null);
        if (roleStatus !== 'STUDENT') {
            document.getElementById('customFile').value = "";
            var fileName = "Choose file";
            $(".custom-file-input").siblings(".custom-file-label").addClass("selected").html(fileName);
        }

        setonlineResourceListData(null);

        var onlineResourceDto = {
            "classId": classDTO.id,
            "subjectId": subjectDTO.id,
            "board": onlineResourceData.board,
            "contentType": onlineResourceData.content
        }

        console.log(onlineResourceDto, "OnlineResourceDtoDto");

        axios.post(baseUrl + '/pdfContent/find', onlineResourceDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setonlineResourceListData(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setonlineResourceListData(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setonlineResourceListData(null);
                handleClick();
            })

    }

    const uploadOnlineResource = () => {

        var onlineResourceDto = {
            "classId": classDTO.id,
            "subjectId": subjectDTO.id,
            "board": onlineResourceData.board,
            "contentType": onlineResourceData.content
        }

        if (onlineResourceData.content !== 'E_BOOK') {
            onlineResourceDto.examId = examDTO.id;
            onlineResourceDto.year = onlineResourceDate.getFullYear();
        }

        if (onlineResourceListData === null) {
            onlineResourceDto.type = "CREATE";
            onlineResourceDto.id = null;
        }
        else {
            onlineResourceDto.type = "EDIT";
            onlineResourceDto.id = onlineResourceListData[0].id;
        }

        let formData = new FormData();
        formData.append('pdfContentDTO', JSON.stringify(onlineResourceDto));
        formData.append('file', fileData);

        console.log(formData, onlineResourceDto, JSON.stringify(onlineResourceDto));

        axios.post(baseUrl + '/pdfContent', formData, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'content-type': 'multipart/form-data', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: true });
                    seteditState({ ...editState, status: false });
                    setfileData(null);
                    document.getElementById('customFile').value = "";
                    setonlineResourceListData(null);
                    var fileName = "Choose file";
                    $(".custom-file-input").siblings(".custom-file-label").addClass("selected").html(fileName);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                }
                handleClick();
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                handleClick();
            })

    }

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
                        <h5 style={{ color: "#B99E01" }}><i style={{ fontSize: "20px" }} className="fa fa-window-restore"></i>&nbsp;&nbsp;Online <span style={{ color: "black" }}>Resource</span></h5><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <FormControl style={{ width: "100%" }}>
                            <InputLabel id="demo-simple-select-label">Content</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={onlineResourceData.content}
                                onChange={(e, child) => getContentData(e, child)}
                            >
                                {
                                    contentType.map((option, index) => (
                                        <MenuItem id={index} key={option.id} value={option.name} >
                                            {option.name}
                                        </MenuItem>
                                    ))}
                            </Select>
                        </FormControl><br /><br /><br /><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <FormControl style={{ width: "100%" }}>
                            <InputLabel id="demo-simple-select-label">Board</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={onlineResourceData.board}
                                onChange={(e, child) => getBoardData(e, child)}
                            >
                                {
                                    boardType.map((option, index) => (
                                        <MenuItem id={index} key={option.id} value={option.name} >
                                            {option.name}
                                        </MenuItem>
                                    ))}
                            </Select>
                        </FormControl><br /><br /><br /><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <FormControl style={{ width: "100%" }}>
                            <InputLabel id="demo-simple-select-label">Class</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={onlineResourceData.class}
                                onChange={(e, child) => getClassData(e, child)}
                            >
                                {
                                    classListData === null ? null :
                                        classListData.data.map((option, index) => (
                                            <MenuItem id={index} key={option.id} value={option.name} >
                                                {option.name}
                                            </MenuItem>
                                        ))}
                            </Select>
                        </FormControl><br /><br /><br /><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {
                            onlineResourceData.class === '' ? null :
                                <div>
                                    <FormControl style={{ width: "100%" }}>
                                        <InputLabel id="demo-simple-select-label">Subject</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={onlineResourceData.subject}
                                            onChange={(e, child) => getSubjectData(e, child)}
                                        >
                                            {
                                                subjectNameList === null ? null :
                                                    subjectNameList.data.map((option, index) => (
                                                        <MenuItem id={index} key={option.id} value={option.name}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                        </Select>
                                    </FormControl><br /><br /><br /><br />
                                </div>
                        }
                    </div>
                </div>

                <div className="row">
                    {
                        (onlineResourceData.content === 'E_BOOK' || onlineResourceData.content === '') ? null :
                            <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <FormControl style={{ width: "100%" }}>
                                    <InputLabel id="demo-simple-select-label">Exam List</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={onlineResourceData.exam}
                                        onChange={(e, child) => getExamData(e, child)}
                                    >
                                        {
                                            examNameList === null ? null :
                                                examNameList.map((option, index) => (
                                                    <MenuItem id={index} key={option.id} value={option.name} >
                                                        {option.name}
                                                    </MenuItem>
                                                ))}
                                    </Select>
                                </FormControl><br /><br /><br /><br />
                            </div>
                    }
                    {
                        (onlineResourceData.content === 'E_BOOK' || onlineResourceData.content === '') ? null :
                            <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <DatePicker
                                    selected={onlineResourceDate}
                                    onChange={date => setonlineResourceDate(date)}
                                    dateFormat="yyyy/MM/dd"
                                    peekNextMonth
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                /><br /><br />
                            </div>
                    }
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {
                            onlineResourceData.content === 'E_BOOK' ?
                                <div>
                                    <Button disabled={
                                        onlineResourceData.class === '' ||
                                        onlineResourceData.subject === '' ||
                                        onlineResourceData.content === '' ||
                                        onlineResourceData.board === ''
                                    } onClick={fetchOnlineResource} style={{ fontSize: "100%" }} variant="contained" color="primary">Fetch</Button><br /><br />
                                </div> :
                                <div>
                                    <Button disabled={
                                        onlineResourceData.class === '' ||
                                        onlineResourceData.subject === '' ||
                                        onlineResourceData.content === '' ||
                                        onlineResourceData.board === '' ||
                                        onlineResourceData.exam === ''
                                    } onClick={fetchOnlineResource} style={{ fontSize: "100%" }} variant="contained" color="primary">Fetch</Button><br /><br />
                                </div>
                        }
                    </div>
                </div>

                {
                    roleStatus !== 'STUDENT' ?
                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div className="custom-file mb-3">
                                    <input accept=".pdf" type="file" onChange={uploadFile}
                                        className="custom-file-input" id="customFile" name="filename" />
                                    <label className="custom-file-label" htmlFor="customFile">Choose file</label>
                                </div>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                {
                                    onlineResourceData.content === 'E_BOOK' ?
                                        <div>
                                            <Button disabled={
                                                onlineResourceData.class === '' ||
                                                onlineResourceData.subject === '' ||
                                                onlineResourceData.content === '' ||
                                                onlineResourceData.board === '' ||
                                                fileData === null
                                            } onClick={uploadOnlineResource} style={{ fontSize: "100%" }} variant="contained" color="primary">Upload</Button>
                                        </div> :
                                        <div>
                                            <Button disabled={
                                                onlineResourceData.class === '' ||
                                                onlineResourceData.subject === '' ||
                                                onlineResourceData.content === '' ||
                                                onlineResourceData.board === '' ||
                                                onlineResourceData.exam === '' ||
                                                fileData === null
                                            } onClick={uploadOnlineResource} style={{ fontSize: "100%" }} variant="contained" color="primary">Upload</Button>
                                        </div>
                                }
                                {
                                    editState.status === false ? null :
                                        <div>
                                            <br /><Button onClick={removeEditMode} style={{ fontSize: "70%" }} variant="contained" color="secondary">Remove EditMode</Button>
                                        </div>
                                }
                            </div>

                        </div>
                        : null
                }


                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center"><br /><br />
                        {
                            onlineResourceListData === null ? (
                                <h5>No Online Resources Available !!</h5>
                            ) : null
                        }
                    </div>
                </div>
                {
                    onlineResourceListData === null ? null :

                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6"><br /><br />

                                <table className="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Sl.No</th>
                                            {
                                                roleStatus !== 'STUDENT' ?
                                                    <th>Edit/View</th> : <th>View</th>
                                            }
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            onlineResourceListData.map((data, index) => (
                                                <tr key={data.id}>
                                                    <td>{index + 1}</td>
                                                    <td>
                                                        {
                                                            editState.status === true ? null :
                                                                <span>
                                                                    {
                                                                        roleStatus !== 'STUDENT' ?
                                                                            <span>
                                                                                <Button onClick={() => editMediaFiles()} style={{ fontSize: "70%" }} variant="contained" color="primary">Edit</Button>&nbsp;&nbsp;
                                                                            </span>
                                                                            : null
                                                                    }
                                                                </span>
                                                        }
                                                        <Button onClick={() => viewMediaFiles(data)} style={{ fontSize: "70%" }} variant="contained" color="secondary">View</Button>
                                                    </td>
                                                </tr>
                                            ))}
                                    </tbody>
                                </table>

                            </div>
                        </div>
                }
            </div>
            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div >
    )
}

export default OnlineResource


