import React, { useState, useEffect } from 'react';
import "./TeachersDashboard.css";
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import axios from 'axios';
import { baseUrl } from '../../../ServerUrls';
import { useHistory } from "react-router-dom";
import Snackbar from '../../../SnackBar/SnackBar';
import Avatar from 'react-avatar';
import StepProgress from '../../../StepProgress/StepProgress';

function TeachersDashboard() {

    let history = useHistory();

    const [pageNumber, setpageNumber] = useState(0);
    const [limit, setlimit] = useState(100);
    const [teachersData, setteachersData] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: false });

    const [open, setOpen] = React.useState(false);

    let roleStatus = localStorage.getItem('Role')

    console.log(roleStatus, "RoleStatus");

    useEffect(() => {

        var teacherDto = {
            "instituteId": localStorage.getItem('InstituteId'),
            "pageNumber": pageNumber,
            "limit": limit,
            "keyword": null
        }

        axios.post(baseUrl + '/teacher/list', teacherDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setteachersData(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setteachersData(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setteachersData(null);
                handleClick();
            })

    }, []);


    const addOrEditTeacher = (status, data) => {
        history.push(
            {
                pathname: "/dashboard/institute/teacher/add",
                state: {
                    teacherType: status,
                    data: data
                }
            }
        )
    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    return (
        <div>
            <StepProgress /><br /><br />
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
                        <h5 style={{ color: "#B99E01" }}><i className="fas fa-chalkboard-teacher"></i>&nbsp;&nbsp;Teachers<span style={{ color: "black" }}>Dashboard</span></h5><br />
                        {
                            roleStatus === 'INSTITUTE-ADMIN' ?
                                <div>
                                    <Fab onClick={() => addOrEditTeacher('CREATE', null)} size="small" color="primary" aria-label="add">
                                        <AddIcon />
                                    </Fab>&nbsp;&nbsp;Add Teachers<br /><br /><br /><br />
                                </div>
                                : null
                        }
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>
                {teachersData === null ? (
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center">
                            <h5>No Teachers Available !!</h5>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                    </div>
                ) :
                    (
                        <div className="row">
                            {teachersData.data.map((data, index) => (
                                <div key={data.id} className="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                    <div id="box" className="card" style={{ width: "100%", borderRadius: "12px" }}>
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                                                    <Avatar name={data.firstName + ' ' + data.lastName} size="35" round={true} />
                                                </div>
                                                <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                    <h5 style={{ marginTop: "4px" }} className="card-title">{data.firstName + ' ' + data.lastName}</h5>
                                                </div>
                                                <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                    {
                                                        roleStatus === 'INSTITUTE-ADMIN' ?
                                                            <div>
                                                                <Fab onClick={() => addOrEditTeacher('EDIT', data)} color="secondary" size="small" aria-label="edit">
                                                                    <EditIcon />
                                                                </Fab>
                                                            </div>
                                                            : null
                                                    }
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                                                <div className="col-xs-12 col-sm-12 col-md-11 col-lg-11">
                                                    <h5>Reg No : {data.registrationNumber}</h5>
                                                    <h5>Mobile Number : {data.mobileNumber}</h5>
                                                    <h5>Address : {data.address}</h5>
                                                    <h5>Gender : {data.gender}</h5>
                                                    <h5>Date Of Birth : {data.dateOfBirth}</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div><br />
                                </div>
                            ))}
                        </div>
                    )
                }
            </div>
            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div>
    )
}

export default TeachersDashboard
