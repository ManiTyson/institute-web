import React, { useState, useEffect } from 'react';
import "./Onboard.css";
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { GoogleComponent } from 'react-google-location';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import { baseUrl } from '../../../ServerUrls';
import { useHistory, useLocation } from "react-router-dom";
import Snackbar from '../../../SnackBar/SnackBar';
import StepProgress from '../../../StepProgress/StepProgress';


const useStyles = makeStyles((theme) => ({
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: 2,
    }
}));

function Onboard(props) {

    const classes = useStyles();

    let location = useLocation();
    let history = useHistory();

    const API_KEY = "cool";  // how to get key - step are below

    const workingDays = [
        {
            id: 1,
            name: 'Monday'
        },
        {
            id: 2,
            name: 'Tuesday'
        },
        {
            id: 3,
            name: 'Wednesday'
        },
        {
            id: 4,
            name: 'Thursday'
        },
        {
            id: 5,
            name: 'Friday'
        },
        {
            id: 6,
            name: 'Saturday'
        },
        {
            id: 7,
            name: 'Sunday'
        }
    ];

    const [onboardData, setonboardData] = useState({
        instituteName: '',
        instituteType: '',
        ownerName: '',
        workingDays: [],
        startHour: new Date(),
        closeHour: new Date(),
        location: {
            lat: "12.945707",
            lng: "79.224118"
        }
    });
    const [allInstituteTypes, setallInstituteTypes] = useState([]);
    const [allWorkingDays, setallWorkingDays] = useState([]);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: true });

    const [open, setOpen] = React.useState(false);

    const data = Object.keys(props.metaData).length === 0;

    const getOnBoardData = (e) => {
        setonboardData({ ...onboardData, [e.target.name]: e.target.value });
        console.log(onboardData);
    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const instituteOnboardSubmit = () => {

        var payload = {
            "instituteName": onboardData.instituteName,
            "instituteType": onboardData.instituteType,
            "ownerName": onboardData.ownerName,
            "mobileNumber": localStorage.getItem('loggedUser'),
            "latitude": onboardData.location.lat,
            "longitude": onboardData.location.lng,
            "workingDays": onboardData.workingDays,
            "openingTime": onboardData.startHour.getHours() + ":" + onboardData.startHour.getMinutes(),
            "closingTime": onboardData.closeHour.getHours() + ":" + onboardData.closeHour.getMinutes(),
            "type": "CREATE"
        }

        console.log(payload);

        axios.post(baseUrl + '/institute', payload, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: true });
                    localStorage.setItem('InstituteId', response.data.data);
                    setTimeout(function () {
                        history.push(
                            {
                                pathname: '/dashboard/institute/teacher',
                            }
                        );
                    }, 2000);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                }
                handleClick();
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                handleClick();
            })

    }

    let roleStatus = localStorage.getItem('Role')

    console.log(roleStatus, "RoleStatus");

    useEffect(() => {
        if (roleStatus === 'INSTITUTE-ADMIN') {
            console.log(props.metaData, "MetaDataProps");
            if (data === true)
                setallInstituteTypes([]);
            else
                setallInstituteTypes(props.metaData.instituteTypeList);
            setallWorkingDays(workingDays);
        }
        else {

            history.push(
                {
                    pathname: "/dashboard"
                }
            )

        }
    }, [props.metaData]);

    return (
        <div>
            <StepProgress /><br /><br />
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                    <div style={{ backgroundColor: "#eeeeee", borderRadius: "4px" }} className="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center"><br />
                        <h5 style={{ color: "#B99E01" }}><i className="fa fa-graduation-cap"></i>On<span style={{ color: "black" }}>board</span></h5>
                        <p><span style={{ color: "#B99E01" }}>Welcome ! Please provide your</span> detailsto get onboarded.</p>
                        <form>
                            <TextField name="instituteName" value={onboardData.instituteName} onChange={getOnBoardData} style={{ width: "100%" }} id="standard-basic" label="Institute Name" /><br /><br />
                            <TextField name="ownerName" value={onboardData.ownerName} onChange={getOnBoardData} style={{ width: "100%" }} id="standard-basic" label="Owner Name" /><br /><br />

                            <FormControl style={{ width: "100%" }}>
                                <InputLabel id="demo-simple-select-label">Institute Type</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={onboardData.instituteType}
                                    onChange={getOnBoardData}
                                    inputProps={{
                                        name: "instituteType"
                                    }}
                                // name="instituteType"
                                >
                                    {allInstituteTypes.map((option, index) => (
                                        <MenuItem key={option.id} value={option}>
                                            {option.name}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl><br /><br />
                            <FormControl style={{ width: "100%" }}>
                                <InputLabel id="demo-mutiple-chip-label">Working Days</InputLabel>
                                <Select
                                    labelId="demo-mutiple-chip-label"
                                    id="demo-mutiple-chip"
                                    multiple
                                    value={onboardData.workingDays}
                                    onChange={getOnBoardData}
                                    name="workingDays"
                                    input={<Input id="select-multiple-chip" />}
                                    renderValue={(selected) => (
                                        <div className={classes.chips}>
                                            {selected.map((value) => (
                                                <Chip key={value} label={value} className={classes.chip} />
                                            ))}
                                        </div>
                                    )}
                                >
                                    {allWorkingDays.map((option, index) => (
                                        <MenuItem key={option.id} value={option.name}>
                                            {option.name}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl><br /><br />
                            <span>Start Hours</span>
                            <div className="customDatePickerWidth">
                                <DatePicker
                                    selected={onboardData.startHour}
                                    onChange={date => setonboardData({ ...onboardData, startHour: date })}
                                    showTimeSelect
                                    showTimeSelectOnly
                                    timeIntervals={15}
                                    timeCaption="Time"
                                    dateFormat="h:mm aa"
                                /></div><br />
                            <span>Close Hours</span>
                            <div className="customDatePickerWidth">
                                <DatePicker
                                    selected={onboardData.closeHour}
                                    onChange={date => setonboardData({ ...onboardData, closeHour: date })}
                                    showTimeSelect
                                    showTimeSelectOnly
                                    timeIntervals={15}
                                    timeCaption="Time"
                                    dateFormat="h:mm aa"
                                /></div><br />
                            {
                                roleStatus === 'INSTITUTE-ADMIN' ?
                                    <div>
                                        <p>Location</p>
                                        <GoogleComponent
                                            apiKey={API_KEY}
                                            language={'en'}
                                            country={'country:in|country:us'}
                                            coordinates={true}
                                            locationBoxStyle={'custom-style'}
                                            locationListStyle={'custom-style-list'}
                                            onChange={e => setonboardData({ ...onboardData, location: e })} /><br />
                                    </div>
                                    : null
                            }
                            <Button disabled={
                                onboardData.instituteName === '' ||
                                onboardData.instituteType === '' ||
                                onboardData.onwerName === '' ||
                                onboardData.workingDays.length === 0
                            } onClick={instituteOnboardSubmit} style={{ fontSize: "100%" }} variant="contained" color="primary">Submit</Button><br /><br />
                        </form>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>
            </div>

            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}

        </div>
    )
}

export default Onboard
