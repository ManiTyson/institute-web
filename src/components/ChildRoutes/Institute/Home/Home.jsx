import React from 'react';
import StepProgress from '../../../StepProgress/StepProgress';

function Home() {
    return (
        <div>
            <StepProgress />
        </div>
    )
}

export default Home
