import React, { useState, useEffect } from 'react';
import "./AddTimetable.css";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useHistory, useLocation } from "react-router-dom";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import { baseUrl } from '../../../ServerUrls';
import Snackbar from '../../../SnackBar/SnackBar';

function AddTimetable() {

    const workingDays = [
        {
            id: 1,
            name: 'Monday'
        },
        {
            id: 2,
            name: 'Tuesday'
        },
        {
            id: 3,
            name: 'Wednesday'
        },
        {
            id: 4,
            name: 'Thursday'
        },
        {
            id: 5,
            name: 'Friday'
        },
        {
            id: 6,
            name: 'Saturday'
        },
        {
            id: 7,
            name: 'Sunday'
        }
    ];

    let location = useLocation();
    let history = useHistory();

    const [timetableData, settimetableData] = useState({
        periodName: '',
        periodDay: '',
        teacher: '',
        subject: '',
        startHour: new Date(),
        closeHour: new Date(),
    });

    const [subjectDTO, setsubjectDTO] = useState(null);
    const [subjectData, setsubjectData] = useState(null);

    const [teacherDTO, setteacherDTO] = useState(null);
    const [teachersData, setteachersData] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: true });

    const [open, setOpen] = React.useState(false);

    const [pageNumber, setpageNumber] = useState(0);
    const [limit, setlimit] = useState(100);

    useEffect(() => {
        console.log(history, location);

        var teacherDto = {
            "instituteId": localStorage.getItem('InstituteId'),
            "pageNumber": pageNumber,
            "limit": limit,
            "keyword": null
        }

        axios.post(baseUrl + '/teacher/list', teacherDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setteachersData(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setteachersData(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setteachersData(null);
                handleClick();
            })

        var subjectDto = {
            "classId": location.state.classObj.id,
            "pageNumber": pageNumber,
            "limit": limit,
            "keyword": null
        }

        axios.post(baseUrl + '/subject/list', subjectDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setsubjectData(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setsubjectData(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setsubjectData(null);
                handleClick();
            })

        if (location.state.studentType === 'EDIT') {

            var beginTime = location.state.data.startTime.split(":");

            var closeTime = location.state.data.endTime.split(":");

            console.log(beginTime,closeTime);

            var today = new Date();

            settimetableData({
                ...timetableData,
                periodName: location.state.data.displayName,
                periodDay: location.state.data.day,
                teacher: location.state.data.teacherDTO.firstName + " " + location.state.data.teacherDTO.lastName,
                class: location.state.classObj.name,
                section: location.state.sectionObj.name,
                subject: location.state.data.subjectDTO.name,
                startHour: new Date(today.getFullYear(), today.getMonth(), today.getDate(), beginTime[0], beginTime[1]),
                closeHour: new Date(today.getFullYear(), today.getMonth(), today.getDate(), closeTime[0], closeTime[1]),
            });
            setteacherDTO(location.state.data.teacherDTO);
            setsubjectDTO(location.state.data.subjectDTO);
        }

    }, []);

    const gettimetableData = (e) => {
        settimetableData({ ...timetableData, [e.target.name]: e.target.value });
        console.log(timetableData, teacherDTO, subjectDTO);
    }

    const gettimetableTeacherData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        settimetableData({ ...timetableData, teacher: e.target.value });
        setteacherDTO(teachersData.data[child.props.id]);
        console.log(timetableData, teacherDTO);
    }

    const gettimetableSubjectData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        settimetableData({ ...timetableData, subject: e.target.value });
        setsubjectDTO(subjectData.data[child.props.id]);
        console.log(timetableData, subjectDTO);
    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const addOrEditSubjects = () => {
        history.push(
            {
                pathname: "/dashboard/institute/subject/add",
                state: {
                    data: location.state.classObj
                }
            }
        )
    };

    const addtimetableSubmit = () => {

        var timetableDto = {
            "id": location.state.data === null ? null : location.state.data.id,
            "instituteId": localStorage.getItem('InstituteId'),
            "classId": location.state.classObj.id,
            "sectionId": location.state.sectionObj.id,
            "teacherId": teacherDTO.id,
            "day": timetableData.periodDay,
            "subjectId": subjectDTO.id,
            "startTime": timetableData.startHour.getHours() + ":" + timetableData.startHour.getMinutes(),
            "endTime": timetableData.closeHour.getHours() + ":" + timetableData.closeHour.getMinutes(),
            "displayName": timetableData.periodName,
            "type": location.state.studentType
        }

        console.log(timetableDto, timetableData);

        axios.post(baseUrl + '/classSchedule', timetableDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: true });
                    setTimeout(function () {
                        history.push(
                            {
                                pathname: '/dashboard/institute/timetable',
                            }
                        );
                    }, 2000);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                }
                handleClick();
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                handleClick();
            })
    }

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <h5><span style={{ color: "#B99E01"}}>Class : </span>{location.state.classObj.name}</h5><br/>
                        <h5><span style={{ color: "#B99E01"}}>Section: </span>{location.state.sectionObj.name}</h5>
                    </div>
                    <div style={{ backgroundColor: "#eeeeee", borderRadius: "4px" }} className="col-xs-12 col-sm-12 col-md-6 col-lg-6"><br />
                        <h5 style={{ color: "#B99E01", textAlign: "center" }}><i className="fa fa-calendar"></i>&nbsp;&nbsp;Timetable<span style={{ color: "black" }}>Details</span></h5><br />
                        <TextField name="periodName" value={timetableData.periodName} onChange={gettimetableData} style={{ width: "100%" }} label="Period Name" /><br /><br />
                        <FormControl style={{ width: "100%" }}>
                            <InputLabel id="demo-simple-select-label">Period Day</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={timetableData.periodDay}
                                onChange={gettimetableData}
                                inputProps={{
                                    name: "periodDay"
                                }}
                            // name="instituteType"
                            >
                                {workingDays.map((option, index) => (
                                    <MenuItem key={option.id} value={option.name}>
                                        {option.name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl><br /><br />
                        <FormControl style={{ width: "100%" }}>
                            <InputLabel id="demo-simple-select-label">Teacher</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={timetableData.teacher}
                                onChange={(e, child) => gettimetableTeacherData(e, child)}
                                inputProps={{
                                    name: "teacher"
                                }}
                            >
                                {
                                    teachersData === null ? null :
                                        teachersData.data.map((option, index) => (
                                            <MenuItem id={index} key={option.id} value={option.firstName + " " + option.lastName} >
                                                {option.firstName + " " + option.lastName}
                                            </MenuItem>
                                        ))}
                            </Select>
                        </FormControl><br /><br />
                        <FormControl style={{ width: "100%" }}>
                            <InputLabel id="demo-simple-select-label">Subject</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={timetableData.subject}
                                onChange={(e, child) => gettimetableSubjectData(e, child)}
                            >
                                {
                                    subjectData === null ? null :
                                        subjectData.data.map((option, index) => (
                                            <MenuItem id={index} key={option.id} value={option.name}>
                                                {option.name}
                                            </MenuItem>
                                        ))}
                            </Select>
                        </FormControl><br /><br />
                        <span>Start Hours</span>
                        <div className="customDatePickerWidth">
                            <DatePicker
                                selected={timetableData.startHour}
                                onChange={date => settimetableData({ ...timetableData, startHour: date })}
                                showTimeSelect
                                showTimeSelectOnly
                                timeIntervals={15}
                                timeCaption="Time"
                                dateFormat="h:mm aa"
                            /></div><br />
                        <span>Close Hours</span>
                        <div className="customDatePickerWidth">
                            <DatePicker
                                selected={timetableData.closeHour}
                                onChange={date => settimetableData({ ...timetableData, closeHour: date })}
                                showTimeSelect
                                showTimeSelectOnly
                                timeIntervals={15}
                                timeCaption="Time"
                                dateFormat="h:mm aa"
                            /></div>
                        <br />
                        <Button disabled={
                            timetableData.periodName === '' ||
                            timetableData.periodDay === '' ||
                            timetableData.teacher === '' ||
                            timetableData.subject === ''
                        } onClick={addtimetableSubmit} style={{ fontSize: "100%" }} variant="contained" color="primary">Submit</Button><br /><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <Button onClick={addOrEditSubjects} style={{ fontSize: "100%" }} variant="contained" color="primary">Add/Edit Subjects</Button>
                    </div>
                </div>
            </div>
            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div>
    )
}

export default AddTimetable


