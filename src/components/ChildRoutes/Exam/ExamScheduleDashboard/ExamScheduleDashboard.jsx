import React, { useState, useEffect } from 'react';
import "./ExamScheduleDashboard.css";
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import axios from 'axios';
import { baseUrl } from '../../../ServerUrls';
import { useHistory, useLocation } from "react-router-dom";
import Snackbar from '../../../SnackBar/SnackBar';
import Avatar from 'react-avatar';
import StepProgress from '../../../StepProgress/StepProgress';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';

function ExamScheduleDashboard() {

    let history = useHistory();
    let location = useLocation();

    const [pageNumber, setpageNumber] = useState(0);
    const [limit, setlimit] = useState(100);

    const [examScheduleData, setexamScheduleData] = useState(null);

    const [examData, setexamData] = useState({
        examName: ''
    });

    const [examNameList, setexamNameList] = useState(null);

    const [classData, setclassData] = useState({
        class: '',
        section: ''
    });

    const [classDTO, setclassDTO] = useState(null);
    const [sectionDTO, setsectionDTO] = useState(null);
    const [examDTO, setexamDTO] = useState(null);

    const [classListData, setclassListData] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: false });

    const [open, setOpen] = React.useState(false);

    let roleStatus = localStorage.getItem('Role')

    console.log(roleStatus, "RoleStatus");

    useEffect(() => {

        console.log(history, location);

        var classDto = {
            "instituteId": localStorage.getItem('InstituteId'),
            "pageNumber": pageNumber,
            "limit": limit,
            "keyword": null
        }

        axios.post(baseUrl + '/class/list', classDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setclassListData(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setclassListData(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setclassListData(null);
                handleClick();
            })

        var examDto = {
            "instituteId": localStorage.getItem('InstituteId')
        }

        axios.post(baseUrl + '/exam/list', examDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setexamNameList(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setexamNameList(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setexamNameList(null);
                handleClick();
            })


    }, []);


    const addOrEditExamSchedule = (status, data) => {

        history.push(
            {
                pathname: "/dashboard/examSchedule/add",
                state: {
                    examScheduleType: status,
                    data: data,
                    examObj: examDTO,
                    classObj: classDTO,
                    sectionObj: sectionDTO
                }
            }
        )
    }

    const getClassData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        if (classData.class !== '') {
            setclassData({ ...classData, class: e.target.value, section: '' });
            setsectionDTO(null);
        }
        else {
            setclassData({ ...classData, class: e.target.value });
        }
        setclassDTO(classListData.data[child.props.id]);
        console.log(classData, classDTO, sectionDTO);
    }

    const getSectionData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        setclassData({ ...classData, section: e.target.value });
        setsectionDTO(classDTO.sections[child.props.id]);
        console.log(classData, classDTO, sectionDTO);
    }

    const getExamData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        setexamData({ ...examData, examName: e.target.value });
        setexamDTO(examNameList[child.props.id]);
        console.log(examData, examDTO);
    }

    const fetchExamSchedule = () => {

        var examScheduleDto = {
            "classId": classDTO.id,
            "sectionId": sectionDTO.id,
            "examId": examDTO.id
        }

        axios.post(baseUrl + '/examSchedule/list', examScheduleDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setexamScheduleData(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setexamScheduleData(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setexamScheduleData(null);
                handleClick();
            })

    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    return (
        <div>
            <StepProgress /><br /><br />
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
                        <h5 style={{ color: "#B99E01" }}><i className="fa fa-pencil-square-o"></i>&nbsp;&nbsp;ExamSchedule<span style={{ color: "black" }}>Dashboard</span></h5><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>


                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <FormControl style={{ width: "100%" }}>
                            <InputLabel id="demo-simple-select-label">Exam List</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={examData.examName}
                                onChange={(e, child) => getExamData(e, child)}
                            >
                                {
                                    examNameList === null ? null :
                                        examNameList.map((option, index) => (
                                            <MenuItem id={index} key={option.id} value={option.name} >
                                                {option.name}
                                            </MenuItem>
                                        ))}
                            </Select>
                        </FormControl><br /><br /><br /><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <FormControl style={{ width: "100%" }}>
                            <InputLabel id="demo-simple-select-label">Class</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={classData.class}
                                onChange={(e, child) => getClassData(e, child)}
                            >
                                {
                                    classListData === null ? null :
                                        classListData.data.map((option, index) => (
                                            <MenuItem id={index} key={option.id} value={option.name} >
                                                {option.name}
                                            </MenuItem>
                                        ))}
                            </Select>
                        </FormControl><br /><br /><br /><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {
                            classData.class === '' ? null :
                                <div>
                                    <FormControl style={{ width: "100%" }}>
                                        <InputLabel id="demo-simple-select-label">Section</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={classData.section}
                                            onChange={(e, child) => getSectionData(e, child)}
                                        >
                                            {
                                                classDTO.sections === null ? null :
                                                    classDTO.sections.map((option, index) => (
                                                        <MenuItem id={index} key={option.id} value={option.name}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                        </Select>
                                    </FormControl><br /><br /><br /><br />
                                </div>
                        }
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                        {
                            classData.class === '' ? null :
                                <div>
                                    <Button disabled={
                                        classData.class === '' ||
                                        classData.section === '' ||
                                        examData.examName === ''
                                    } onClick={fetchExamSchedule} style={{ fontSize: "100%" }} variant="contained" color="primary">Fetch</Button><br /><br />
                                </div>
                        }
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        {
                            examScheduleData === null ? (
                                <h5>No Exam Schedule Data Available !!</h5>
                            ) : null
                        }
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {
                            classData.section === '' ? null :
                                examData.examName === '' ? null :
                                    <div>
                                        {
                                            roleStatus !== 'STUDENT' ?
                                                <div>
                                                    <Fab onClick={() => addOrEditExamSchedule('CREATE', null)} size="small" color="primary" aria-label="add">
                                                        <AddIcon />
                                                    </Fab>&nbsp;&nbsp;Add ExamSchedule<br /><br /><br /><br />
                                                </div>
                                                : null
                                        }
                                    </div>
                        }
                    </div>
                </div>

                {examScheduleData === null ? null :
                    (
                        <div className="row">
                            {examScheduleData.map((data, index) => (
                                <div key={data.id} className="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                    <div id="box" className="card" style={{ width: "100%", borderRadius: "12px" }}>
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                                                    <Avatar name={data.displayName} size="35" round={true} />
                                                </div>
                                                <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                    <h5 style={{ marginTop: "4px" }} className="card-title">{data.displayName}</h5>
                                                </div>
                                                <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                    {
                                                        roleStatus !== 'STUDENT' ?
                                                            <div>
                                                                <Fab onClick={() => addOrEditExamSchedule('EDIT', data)} color="secondary" size="small" aria-label="edit">
                                                                    <EditIcon />
                                                                </Fab>
                                                            </div>
                                                            : null
                                                    }
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                                                <div className="col-xs-12 col-sm-12 col-md-11 col-lg-11">
                                                    <h5>Subject: {data.subject.name}</h5>
                                                    <h5>Date: {data.date}</h5>
                                                    <h5>{data.displayStartTime + " -  " + data.displayEndTime}</h5>
                                                    <h5>Total Marks : {data.totalMark}</h5>
                                                    <h5>Pass Marks : {data.passMark}</h5>
                                                    {
                                                        data.comments === null ? null :
                                                            <h5>Comments : {data.comments}</h5>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div><br />
                                </div>
                            ))}
                        </div>
                    )
                }
            </div>
            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div>
    )
}

export default ExamScheduleDashboard


