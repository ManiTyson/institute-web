import React, { useState, useEffect } from 'react';
import "./ExamDashboard.css";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useHistory, useLocation } from "react-router-dom";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import { baseUrl } from '../../../ServerUrls';
import Snackbar from '../../../SnackBar/SnackBar';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import { Modal, ModalHeader, ModalFooter, ModalBody } from 'reactstrap';

function ExamDashboard() {

    let location = useLocation();
    let history = useHistory();

    const [examData, setexamData] = useState({
        examName: ''
    });

    const [examNameList, setexamNameList] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: true });

    const [open, setOpen] = React.useState(false);

    const [exam, setexam] = useState(false);

    const [callUseEffect, setcallUseEffect] = useState(false);

    const [editexam, seteditexam] = useState(false);

    const [editexamField, seteditexamField] = useState('');

    const [pageNumber, setpageNumber] = useState(0);
    const [limit, setlimit] = useState(100);

    let roleStatus = localStorage.getItem('Role')

    console.log(roleStatus, "RoleStatus");

    useEffect(() => {
        console.log(history, location);

        var examDto = {
            "instituteId": localStorage.getItem('InstituteId')
        }

        axios.post(baseUrl + '/exam/list', examDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setexamNameList(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setexamNameList(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setexamNameList(null);
                handleClick();
            })

    }, [callUseEffect]);

    const getexamData = (e) => {
        setexamData({ ...examData, [e.target.name]: e.target.value });
        console.log(examData, examNameList);
    }

    const addexam = () => {
        setexam(true);
    }

    const editexamFieldHandler = (e) => {
        seteditexamField({ ...editexamField, name: e.target.value });
        console.log(editexamField);
    };

    const editexamData = (section) => {
        seteditexamField(section);
        seteditexam(true);
    };

    const toggleexamModal = () => {
        seteditexam(!editexam);
    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const addOrEditexamSubmit = (type) => {

        var examDto = {
            "instituteId": localStorage.getItem('InstituteId'),
            "name": type === "CREATE" ? examData.examName : editexamField.name,
            "id": type === "CREATE" ? null : editexamField.id,
            "type": type
        }

        console.log(examDto);

        axios.post(baseUrl + '/exam', examDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==' } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: true });
                    setcallUseEffect(!callUseEffect);
                    if (type === "EDIT")
                        toggleexamModal();
                    setexamData({ ...examData, examName: '' });
                    setexam(false);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                }
                handleClick();
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                handleClick();
            })
    }

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-5 col-lg-5"></div>
                    <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <h5 style={{ color: "#B99E01" }}><i style={{ fontSize: "25px" }} className="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;Exam<span style={{ color: "black" }}>Details</span></h5><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                    <div style={{ borderRadius: "4px" }} className="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center"><br />
                        {
                            roleStatus !== 'STUDENT' ?
                                <div>
                                    <Fab onClick={addexam} size="small" color="primary" aria-label="add">
                                        <AddIcon />
                                    </Fab>&nbsp;&nbsp;Add exam<br /><br />
                                </div>
                                : null
                        }
                        {exam === false ? null :
                            <div>
                                <TextField name="examName" value={examData.examName} onChange={getexamData} style={{ width: "60%" }} label="Exam Name" />
                                <Button disabled={
                                    examData.examName === ''
                                } onClick={() => addOrEditexamSubmit("CREATE")} style={{ fontSize: "100%" }} variant="contained" color="secondary">Add</Button>
                            </div>
                        }<br /><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>
                {(examNameList === null || examNameList.length === 0) ? (
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center">
                            <h5>No Exams Available !!</h5>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                    </div>
                ) :
                    (
                        <div className="row">
                            {examNameList.map((data, index) => (
                                <div key={data.id} className="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center"><br /><br />
                                    <div id="box" className="card" style={{ width: "100%", borderRadius: "12px" }}>
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                    {data.name}
                                                </div>
                                                <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                    {
                                                        roleStatus !== 'STUDENT' ?
                                                            <div>
                                                                <Fab onClick={() => editexamData(data)} color="secondary" size="small" aria-label="edit">
                                                                    <EditIcon />
                                                                </Fab>
                                                            </div>
                                                            : null
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                    )
                }
            </div>

            {/* ModalSection */}
            <Modal style={{ marginTop: "200px" }} className="my-modal" isOpen={editexam} toggle={toggleexamModal}>
                <ModalHeader cssModule={{ 'modal-title': 'w-100 text-center' }} toggle={toggleexamModal}>
                    <Button style={{ fontSize: "60%" }} variant="contained" color="primary">exam</Button>
                </ModalHeader>
                <ModalBody>
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                        <div className="col-xs-12 col-sm-12 col-md-7 col-lg-7 text-center">
                            <TextField value={editexamField.name} onChange={editexamFieldHandler} style={{ width: "60%" }} label="Section Name" />
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-center">
                            <Button disabled={editexamField.name === ''} onClick={() => addOrEditexamSubmit("EDIT")} style={{ fontSize: "90%" }} variant="contained" color="primary">Edit</Button>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button style={{ fontSize: "70%" }} variant="contained" color="secondary" onClick={toggleexamModal}>Cancel</Button>
                </ModalFooter>
            </Modal>

            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div>
    )
}

export default ExamDashboard



