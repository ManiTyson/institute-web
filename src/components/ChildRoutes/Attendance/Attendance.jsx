import React, { useState, useEffect } from 'react';
import "./Attendance.css";
import Fab from '@material-ui/core/Fab';
import EditIcon from '@material-ui/icons/Edit';
import axios from 'axios';
import { baseUrl } from '../../ServerUrls';
import { useHistory } from "react-router-dom";
import Snackbar from '../../SnackBar/SnackBar';
import Avatar from 'react-avatar';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import DatePicker from "react-datepicker";
import TextField from '@material-ui/core/TextField';
import "react-datepicker/dist/react-datepicker.css";

function Attendance() {

    let history = useHistory();

    const [pageNumber, setpageNumber] = useState(0);
    const [limit, setlimit] = useState(100);

    const [studentsData, setstudentsData] = useState(null);

    const [classListData, setclassListData] = useState(null);
    const [classData, setclassData] = useState({
        class: '',
        section: '',
        date: new Date()
    });

    const collegeStatus = [
        {
            id: 1,
            name: 'WORKING_DAY'
        },
        {
            id: 2,
            name: 'HALF_DAY'
        },
        {
            id: 3,
            name: 'HOLIDAY'
        }
    ];

    const studentStatus = [
        {
            id: 1,
            name: 'PRESENT'
        },
        {
            id: 2,
            name: 'ABSENT'
        },
        {
            id: 3,
            name: 'HALF_DAY'
        },
        {
            id: 4,
            name: 'LEAVE'
        }
    ];

    const [classDTO, setclassDTO] = useState(null);
    const [sectionDTO, setsectionDTO] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: false });

    const [open, setOpen] = React.useState(false);

    let roleStatus = localStorage.getItem('Role')

    console.log(roleStatus, "RoleStatus");

    useEffect(() => {

        if (roleStatus !== 'STUDENT') {

            var classDto = {
                "instituteId": localStorage.getItem('InstituteId'),
                "pageNumber": pageNumber,
                "limit": limit,
                "keyword": null
            }

            axios.post(baseUrl + '/class/list', classDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
                .then(response => {
                    console.log(response);
                    if (response.data.success === true) {
                        setclassListData(response.data.data);
                    }
                    else {
                        setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                        setclassListData(null);
                        handleClick();
                    }
                })
                .catch(error => {
                    console.log(error, error.response, error.message, error.request);
                    setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                    setclassListData(null);
                    handleClick();
                })
        }
        else {

            history.push(
                {
                    pathname: "/dashboard"
                }
            )

        }

    }, []);

    const getClassData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        if (classData.class !== '') {
            setclassData({ ...classData, class: e.target.value, section: '' });
            setsectionDTO(null);
        }
        else {
            setclassData({ ...classData, class: e.target.value });
        }
        setclassDTO(classListData.data[child.props.id]);
        console.log(classData, classDTO, sectionDTO);
    }

    const getSectionData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        setclassData({ ...classData, section: e.target.value });
        setsectionDTO(classDTO.sections[child.props.id]);
        console.log(classData, classDTO, sectionDTO);
    }

    const getStudentsData = (e) => {
        setstudentsData({ ...studentsData, [e.target.name]: e.target.value });
        for (var i = 0; i < studentsData.attendanceTrackerDetailList.length; i++) {
            var data = studentsData.attendanceTrackerDetailList[i];
            if (data.status === null) {
                data.status = "PRESENT";
            }
        }
        console.log(studentsData);
    }

    const changeAttendanceStatus = (e, index) => {
        let newData = { ...studentsData };
        console.log(e.target.value, index, newData);
        newData.attendanceTrackerDetailList[index].status = e.target.value;
        setstudentsData(newData);
        console.log(studentsData);
    }

    const changeAttendanceComments = (e, index) => {
        let newData = { ...studentsData };
        console.log(e.target.value, index, newData);
        newData.attendanceTrackerDetailList[index].comments = e.target.value;
        setstudentsData(newData);
        console.log(studentsData);
    }

    const fetchStudentsData = () => {

        var attendanceListingDto = {
            "instituteId": localStorage.getItem('InstituteId'),
            "classId": classDTO.id,
            "sectionId": sectionDTO.id,
            "date": classData.date
        }

        console.log(attendanceListingDto);

        axios.post(baseUrl + '/attendanceTrackerDetail/student/attendanceList', attendanceListingDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setstudentsData(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setstudentsData(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setstudentsData(null);
                handleClick();
            })

    }


    const updateAttendance = () => {

        axios.post(baseUrl + '/attendanceTrackerDetail', studentsData, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: true });
                    handleClick();
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                handleClick();
            })

    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                    <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
                        <h5 style={{ color: "#B99E01" }}><i style={{ fontSize: "20px" }} className="fa fa-clock-o"></i>&nbsp;&nbsp;Attendance<span style={{ color: "black" }}>Dashboard</span></h5><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                        <DatePicker
                            selected={classData.date}
                            onChange={date => setclassData({ ...classData, date: date })}
                            dateFormat="yyyy/MM/dd"
                            peekNextMonth
                            showMonthDropdown
                            showYearDropdown
                            dropdownMode="select"
                        />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <FormControl style={{ width: "100%" }}>
                            <InputLabel id="demo-simple-select-label">Class</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={classData.class}
                                onChange={(e, child) => getClassData(e, child)}
                            >
                                {
                                    classListData === null ? null :
                                        classListData.data.map((option, index) => (
                                            <MenuItem id={index} key={option.id} value={option.name} >
                                                {option.name}
                                            </MenuItem>
                                        ))}
                            </Select>
                        </FormControl>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {
                            classData.class === '' ? null :
                                <div>
                                    <FormControl style={{ width: "100%" }}>
                                        <InputLabel id="demo-simple-select-label">Section</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={classData.section}
                                            onChange={(e, child) => getSectionData(e, child)}
                                        >
                                            {
                                                classDTO.sections === null ? null :
                                                    classDTO.sections.map((option, index) => (
                                                        <MenuItem id={index} key={option.id} value={option.name}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                        </Select>
                                    </FormControl>
                                </div>
                        }
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {
                            studentsData === null ? null :
                                <div>
                                    <FormControl style={{ width: "100%" }}>
                                        <InputLabel id="demo-simple-select-label">College Status</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={studentsData.status === null ? '' : studentsData.status}
                                            onChange={getStudentsData}
                                            inputProps={{
                                                name: "status"
                                            }}
                                        >
                                            {
                                                collegeStatus === null ? null :
                                                    collegeStatus.map((option, index) => (
                                                        <MenuItem key={option.id} value={option.name}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                        </Select>
                                    </FormControl>
                                </div>
                        }
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                        <Button disabled={
                            classData.class === '' ||
                            classData.section === ''
                        } onClick={fetchStudentsData} style={{ fontSize: "100%" }} variant="contained" color="primary">Fetch</Button>
                    </div>
                </div>
                {
                    studentsData === null ? null :

                        <div>
                            {
                                studentsData.status === null ? null :

                                    <div>
                                        {
                                            studentsData.status === 'HOLIDAY' ?

                                                <div className="row">
                                                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center"><br /><br /><br /><br />
                                                        <h4>Today Holiday For All Students !!</h4><br />
                                                        <TextField value={studentsData.comments === null ? '' : studentsData.comments} onChange={(e) => setstudentsData({ ...studentsData, comments: e.target.value })} style={{ width: "30%" }} id="standard-basic" label="Comments" /><br /><br />
                                                        <Button onClick={updateAttendance} style={{ fontSize: "100%" }} variant="contained" color="primary">Update</Button>
                                                    </div>
                                                </div>

                                                :

                                                <div>
                                                    {
                                                        studentsData.attendanceTrackerDetailList.length === 0 ? null :
                                                            <div className="row">
                                                                <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                                                                <div className="col-xs-12 col-sm-12 col-md-10 col-lg-10"><br /><br /><br /><br />
                                                                    <table className="table table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Name</th>
                                                                                <th>Registration</th>
                                                                                <th>Status</th>
                                                                                <th>Comments</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            {
                                                                                studentsData.attendanceTrackerDetailList.map((data, index) => (
                                                                                    <tr key={data.studentId}>
                                                                                        <td>
                                                                                            <Avatar name={data.studentFirstName + ' ' + data.studentLastName} size="35" round={true} />&nbsp;&nbsp;
                                                                                {data.studentFirstName + ' ' + data.studentLastName}
                                                                                        </td>
                                                                                        <td>{data.registrationNumber}</td>
                                                                                        <td>
                                                                                            <FormControl style={{ width: "150px" }}>
                                                                                                <InputLabel id="demo-simple-select-label">Student Status</InputLabel>
                                                                                                <Select
                                                                                                    labelId="demo-simple-select-label"
                                                                                                    id="demo-simple-select"
                                                                                                    value={data.status === null ? '' : data.status}
                                                                                                    onChange={(e) => changeAttendanceStatus(e, index)}
                                                                                                >
                                                                                                    {
                                                                                                        studentStatus === null ? null :
                                                                                                            studentStatus.map((option, index) => (
                                                                                                                <MenuItem key={option.id} value={option.name}>
                                                                                                                    {option.name}
                                                                                                                </MenuItem>
                                                                                                            ))}
                                                                                                </Select>
                                                                                            </FormControl>
                                                                                        </td>
                                                                                        <td>
                                                                                            <TextField value={data.comments === null ? '' : data.comments} onChange={(e) => changeAttendanceComments(e, index)} style={{ width: "150px" }} id="standard-basic" label="Comments" />
                                                                                        </td>
                                                                                    </tr>
                                                                                ))}
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                                                            </div>
                                                    }

                                                    <div className="row">
                                                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center"><br /><br />
                                                            <Button onClick={updateAttendance} style={{ fontSize: "100%" }} variant="contained" color="primary">Update</Button>
                                                        </div>
                                                    </div>

                                                </div>

                                        }
                                    </div>
                            }

                        </div>

                }
            </div>
            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div >
    )
}

export default Attendance


