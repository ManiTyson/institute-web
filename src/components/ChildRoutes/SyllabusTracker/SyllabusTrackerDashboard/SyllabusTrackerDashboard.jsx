import React, { useState, useEffect } from 'react';
import "./SyllabusTrackerDashboard.css";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useHistory, useLocation } from "react-router-dom";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import { baseUrl } from '../../../ServerUrls';
import Snackbar from '../../../SnackBar/SnackBar';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import { Modal, ModalHeader, ModalFooter, ModalBody } from 'reactstrap';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

function SyllabusTrackerDashboard() {

    let location = useLocation();
    let history = useHistory();

    const [subjectData, setsubjectData] = useState({
        subjectName: ''
    });

    const [subjectNameList, setsubjectNameList] = useState(null);

    const [classData, setclassData] = useState({
        class: '',
        section: ''
    });

    const [classDTO, setclassDTO] = useState(null);
    const [sectionDTO, setsectionDTO] = useState(null);
    const [subjectDTO, setsubjectDTO] = useState(null);

    const [classListData, setclassListData] = useState(null);


    const [syllabusData, setsyllabusData] = useState({
        syllabusChapterName: '',
        syllabusReferenceName: ''
    });

    const [syllabusTrackerList, setsyllabusTrackerList] = useState(null);

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: true });

    const [open, setOpen] = React.useState(false);

    const [syllabusChapter, setsyllabusChapter] = useState(false);

    const [syllabusReference, setsyllabusReference] = useState(false);

    const [editsyllabus, seteditsyllabus] = useState({
        syllabusEditChapterName: false,
        syllabusEditReferenceName: false
    });

    const [editsyllabusField, seteditsyllabusField] = useState({
        syllabusEditChapterName: '',
        syllabusEditReferenceName: '',
        index: ''
    });

    const [pageNumber, setpageNumber] = useState(0);
    const [limit, setlimit] = useState(100);

    const [chapterList, setchapterList] = useState([]);
    const [referenceList, setreferenceList] = useState([]);

    const [mediaFiles, setmediaFiles] = useState({});

    let roleStatus = localStorage.getItem('Role')

    console.log(roleStatus, "RoleStatus");

    useEffect(() => {
        console.log(history, location);

        if (location.state) {
            console.log("Location State", location, location.state);

            setclassDTO(location.state.classObj);
            setsectionDTO(location.state.sectionObj);
            setclassData({ ...classData, class: location.state.classObj.name, section: location.state.sectionObj.name });
            setsubjectDTO(location.state.subjectObj);
            setsubjectData({ ...subjectData, subjectName: location.state.subjectObj.name })
            setchapterList(location.state.chapterList);
            setreferenceList(location.state.referenceList);
            setclassListData(location.state.classListData);
            setsubjectNameList(location.state.subjectNameList);
            setmediaFiles(location.state.mediaFiles);
            setsyllabusTrackerList(location.state.syllabusTrackerList);
        }
        else {
            var classDto = {
                "instituteId": localStorage.getItem('InstituteId'),
                "pageNumber": pageNumber,
                "limit": limit,
                "keyword": null
            }

            axios.post(baseUrl + '/class/list', classDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
                .then(response => {
                    console.log(response);
                    if (response.data.success === true) {
                        setclassListData(response.data.data);
                    }
                    else {
                        setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                        setclassListData(null);
                        handleClick();
                    }
                })
                .catch(error => {
                    console.log(error, error.response, error.message, error.request);
                    setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                    setclassListData(null);
                    handleClick();
                })
        }


    }, []);

    const getsyllabusData = (e) => {
        setsyllabusData({ ...syllabusData, [e.target.name]: e.target.value });
        console.log(syllabusData, syllabusTrackerList);
    }

    const addsyllabusChapter = () => {
        setsyllabusChapter(true);
    }

    const addsyllabusReference = () => {
        setsyllabusReference(true);
    }

    const editsyllabusFieldHandler = (e) => {
        seteditsyllabusField({ ...editsyllabusField, [e.target.name]: e.target.value });
        console.log(editsyllabusField);
    };

    const editsyllabusData = (data, trackerType, index) => {
        if (trackerType === "CHAPTER") {
            seteditsyllabusField({ ...editsyllabusField, syllabusEditChapterName: data.title, index: index });
            seteditsyllabus({ ...editsyllabus, syllabusEditChapterName: true });
        }
        else {
            seteditsyllabusField({ ...editsyllabusField, syllabusEditReferenceName: data.title, index: index });
            seteditsyllabus({ ...editsyllabus, syllabusEditReferenceName: true });
        }
    };

    const togglesyllabusChapterModal = () => {
        seteditsyllabus({ ...editsyllabus, syllabusEditChapterName: !editsyllabus.syllabusEditChapterName });
    }

    const togglesyllabusReferenceModal = () => {
        seteditsyllabus({ ...editsyllabus, syllabusEditReferenceName: !editsyllabus.syllabusEditReferenceName });
    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const getClassData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        if (classData.class !== '') {
            setclassData({ ...classData, class: e.target.value, section: '' });
            setsubjectData({ ...subjectData, subjectName: '' });
            setsectionDTO(null);
            setsubjectDTO(null);
        }
        else {
            setclassData({ ...classData, class: e.target.value });
        }
        setclassDTO(classListData.data[child.props.id]);
        console.log(classData, classDTO, sectionDTO, subjectDTO, chapterList, referenceList);
    }

    const getSectionData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        setclassData({ ...classData, section: e.target.value });
        setsectionDTO(classDTO.sections[child.props.id]);
        console.log(classData, classDTO, sectionDTO);
        subjectListingApi();
    }

    const subjectListingApi = () => {

        var subjectDto = {
            "pageNumber": pageNumber,
            "limit": limit,
            "keyword": null
        }

        if (location.state) {
            subjectDto.classId = location.state.classObj.id
        }
        else {
            subjectDto.classId = classDTO.id
        }

        axios.post(baseUrl + '/subject/list', subjectDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setsubjectNameList(response.data.data);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setsubjectNameList(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setsubjectNameList(null);
                handleClick();
            })

    }

    const getSubjectData = (e, child) => {
        console.log(e.target.value, child.props.id, child);
        setsubjectData({ ...subjectData, subjectName: e.target.value });
        setsubjectDTO(subjectNameList.data[child.props.id]);
        console.log(subjectData, subjectDTO);
    }

    const fetchSyllabusData = () => {

        var syllabusChapterDto = {};

        syllabusChapterDto.classId = classDTO.id;
        syllabusChapterDto.sectionId = sectionDTO.id;
        syllabusChapterDto.subjectId = subjectDTO.id;

        setchapterList([]);
        setreferenceList([]);

        axios.post(baseUrl + '/syllabusTracker/list', syllabusChapterDto, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setsyllabusTrackerList(response.data.data);
                    for (var i = 0; i < response.data.data.syllabusTrackerList.length; i++) {
                        var dataSyllabus = response.data.data.syllabusTrackerList[i];
                        if (dataSyllabus.trackerType === "CHAPTER") {
                            setchapterList(prevChapterList => ([...prevChapterList, dataSyllabus]));
                        }
                        else {
                            setreferenceList(prevReferenceList => ([...prevReferenceList, dataSyllabus]));
                        }
                    }
                    console.log(chapterList, referenceList);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    setsyllabusTrackerList(null);
                    handleClick();
                }
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                setsyllabusTrackerList(null);
                handleClick();
            })

    }

    const addSyllabusSubmit = (trackerType) => {

        if (trackerType === "CHAPTER") {
            setchapterList([...chapterList, {
                "contents": [],
                "title": syllabusData.syllabusChapterName,
                "trackerType": trackerType,
                "priority": chapterList.length + 1,
                "type": "CREATE"
            }])
            setsyllabusData({ ...syllabusData, syllabusChapterName: '' });
            setsyllabusChapter(false);
        }

        else {
            setreferenceList([...referenceList, {
                "contents": [],
                "title": syllabusData.syllabusReferenceName,
                "trackerType": trackerType,
                "priority": referenceList.length + 1,
                "type": "CREATE"
            }])
            setsyllabusData({ ...syllabusData, syllabusReferenceName: '' });
            setsyllabusReference(false);
        }

        console.log(chapterList, referenceList);

    }

    const editSyllabusSubmit = (trackerType) => {

        if (trackerType === "CHAPTER") {
            let newArr = [...chapterList];
            newArr[editsyllabusField.index].title = editsyllabusField.syllabusEditChapterName;
            setchapterList(newArr);
            togglesyllabusChapterModal();
            seteditsyllabusField({ ...editsyllabusField, syllabusEditChapterName: '' });
        }
        else {
            let newArr = [...referenceList];
            newArr[editsyllabusField.index].title = editsyllabusField.syllabusEditReferenceName;
            setreferenceList(newArr);
            togglesyllabusReferenceModal();
            seteditsyllabusField({ ...editsyllabusField, syllabusEditReferenceName: '' });
        }

        console.log(chapterList, referenceList);

    }

    const openSyllabusContent = (data) => {
        console.log(data, "Data");
        history.push(
            {
                pathname: "/dashboard/syllabusTrackerContent/add",
                state: {
                    data: data,
                    chapterList: chapterList,
                    referenceList: referenceList,
                    classObj: classDTO,
                    sectionObj: sectionDTO,
                    subjectObj: subjectDTO,
                    classListData: classListData,
                    subjectNameList: subjectNameList,
                    mediaFiles: mediaFiles,
                    syllabusTrackerList: syllabusTrackerList
                }
            }
        )
    }

    const createSyllabusTracker = () => {



        var syllabusTrackerDto = {
            "classId": classDTO.id,
            "sectionId": sectionDTO.id,
            "subjectId": subjectDTO.id,
            "syllabusTrackerList": [...chapterList, ...referenceList]
        }

        let formData = new FormData();
        formData.append('syllabusTrackerDTO', JSON.stringify(syllabusTrackerDto));
        for (var key in mediaFiles) {
            console.log(key, mediaFiles[key]);
            formData.append(key, mediaFiles[key]);
        }

        console.log(formData, syllabusTrackerDto, JSON.stringify(syllabusTrackerDto));

        axios.post(baseUrl + '/syllabusTracker', formData, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'content-type': 'multipart/form-data', 'userId': localStorage.getItem('UserId') } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: true });
                    setTimeout(function () {
                        history.replace(
                            {
                                pathname: "/dashboard/syllabusTrackerDashboard"
                            }
                        );
                    }, 2000);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                }
                handleClick();
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                handleClick();
            })

    }

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-5 col-lg-5"></div>
                    <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <h5 style={{ color: "#B99E01" }}><i style={{ fontSize: "25px" }} className="fa fa-book" aria-hidden="true"></i>&nbsp;&nbsp;Syllabus Chapter <span style={{ color: "black" }}>Details</span></h5><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3"></div>
                </div>

                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <FormControl style={{ width: "100%" }}>
                            <InputLabel id="demo-simple-select-label">Class</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={classData.class}
                                onChange={(e, child) => getClassData(e, child)}
                            >
                                {
                                    classListData === null ? null :
                                        classListData.data.map((option, index) => (
                                            <MenuItem id={index} key={option.id} value={option.name} >
                                                {option.name}
                                            </MenuItem>
                                        ))}
                            </Select>
                        </FormControl><br /><br /><br /><br />
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {
                            classData.class === '' ? null :
                                <div>
                                    <FormControl style={{ width: "100%" }}>
                                        <InputLabel id="demo-simple-select-label">Section</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={classData.section}
                                            onChange={(e, child) => getSectionData(e, child)}
                                        >
                                            {
                                                classDTO.sections === null ? null :
                                                    classDTO.sections.map((option, index) => (
                                                        <MenuItem id={index} key={option.id} value={option.name}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                        </Select>
                                    </FormControl><br /><br /><br /><br />
                                </div>
                        }
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        {
                            classData.section === '' ? null :
                                <div>
                                    <FormControl style={{ width: "100%" }}>
                                        <InputLabel id="demo-simple-select-label">Subject</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={subjectData.subjectName}
                                            onChange={(e, child) => getSubjectData(e, child)}
                                        >
                                            {
                                                subjectNameList === null ? null :
                                                    subjectNameList.data.map((option, index) => (
                                                        <MenuItem id={index} key={option.id} value={option.name}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                        </Select>
                                    </FormControl><br /><br /><br /><br />
                                </div>
                        }
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                        {
                            classData.class === '' ? null :
                                <div>
                                    <Button disabled={
                                        classData.class === '' ||
                                        classData.section === '' ||
                                        subjectData.subjectName === ''
                                    } onClick={fetchSyllabusData} style={{ fontSize: "100%" }} variant="contained" color="primary">Fetch</Button><br /><br />
                                </div>
                        }
                    </div>
                </div>

                {
                    subjectData.subjectName === '' ? null :
                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                            <div style={{ borderRadius: "4px" }} className="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center"><br />
                                {
                                    roleStatus !== 'STUDENT' ?
                                        <div>
                                            <Fab onClick={addsyllabusChapter} size="small" color="primary" aria-label="add">
                                                <AddIcon />
                                            </Fab>&nbsp;&nbsp;Add Syllabus Chapter<br /><br />
                                        </div>
                                        : null
                                }
                                {syllabusChapter === false ? null :
                                    <div>
                                        <TextField name="syllabusChapterName" value={syllabusData.syllabusChapterName} onChange={getsyllabusData} style={{ width: "60%" }} label="Syllabus Chapter Name" />
                                        <Button disabled={
                                            syllabusData.syllabusChapterName === ''
                                        } onClick={() => addSyllabusSubmit("CHAPTER")} style={{ fontSize: "100%" }} variant="contained" color="secondary">Add</Button>
                                    </div>
                                }<br /><br />
                            </div>
                            <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                            <div style={{ borderRadius: "4px" }} className="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center"><br />
                                {
                                    roleStatus !== 'STUDENT' ?
                                        <div>
                                            <Fab onClick={addsyllabusReference} size="small" color="primary" aria-label="add">
                                                <AddIcon />
                                            </Fab>&nbsp;&nbsp;Add Syllabus Reference<br /><br />
                                        </div>
                                        : null
                                }
                                {syllabusReference === false ? null :
                                    <div>
                                        <TextField name="syllabusReferenceName" value={syllabusData.syllabusReferenceName} onChange={getsyllabusData} style={{ width: "60%" }} label="Syllabus Reference Name" />
                                        <Button disabled={
                                            syllabusData.syllabusReferenceName === ''
                                        } onClick={() => addSyllabusSubmit("REFERENCE")} style={{ fontSize: "100%" }} variant="contained" color="secondary">Add</Button>
                                    </div>
                                }<br /><br />
                            </div>
                        </div>
                }
                {
                    chapterList.length === 0 ? (
                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center">
                                <h5>No Syllabus Chapters Available !!</h5>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                        </div>
                    ) :
                        (
                            <div>
                                <h5 style={{ textAlign: "center" }}>List of Chapters</h5>
                                <div className="row">
                                    {chapterList.map((data, index) => (
                                        <div style={{ cursor: "pointer" }} key={index} className="col-xs-12 col-sm-12 col-md-3 col-lg-3"><br /><br />
                                            <div id="box" className="card" style={{ width: "100%", borderRadius: "12px" }}>
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                            {data.title}
                                                        </div>
                                                        <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                            {
                                                                roleStatus !== 'STUDENT' ?
                                                                    <div>
                                                                        <Fab onClick={() => editsyllabusData(data, "CHAPTER", index)} color="secondary" size="small" aria-label="edit">
                                                                            <EditIcon />
                                                                        </Fab>
                                                                    </div>
                                                                    : null
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <p>No Of Contents : {data.contents.length}</p>
                                                            <Button onClick={() => openSyllabusContent(data)} style={{ fontSize: "60%" }} variant="contained" color="secondary">View</Button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        )
                }

                {
                    referenceList.length === 0 ? (
                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 text-center"><br /><br />
                                <h5>No Syllabus References Available !!</h5>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3"></div>
                        </div>
                    ) :
                        (
                            <div><br /><br />
                                <h5 style={{ textAlign: "center" }}>List of References</h5>
                                <div className="row">
                                    {referenceList.map((data, index) => (
                                        <div key={index} className="col-xs-12 col-sm-12 col-md-3 col-lg-3"><br /><br />
                                            <div id="box" className="card" style={{ width: "100%", borderRadius: "12px" }}>
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                            {data.title}
                                                        </div>
                                                        <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                            {
                                                                roleStatus !== 'STUDENT' ?
                                                                    <div>
                                                                        <Fab onClick={() => editsyllabusData(data, "REFERENCE", index)} color="secondary" size="small" aria-label="edit">
                                                                            <EditIcon />
                                                                        </Fab>
                                                                    </div>
                                                                    : null
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <p>No Of Contents : {data.contents.length}</p>
                                                            <Button onClick={() => openSyllabusContent(data)} style={{ fontSize: "60%" }} variant="contained" color="secondary">View</Button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        )
                }
                {
                    (chapterList.length !== 0 || referenceList.length !== 0) ?
                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center"><br /><br />
                                {
                                    roleStatus !== 'STUDENT' ?
                                        <div>
                                            <Button onClick={createSyllabusTracker} style={{ fontSize: "100%" }} variant="contained" color="primary">
                                                {
                                                    syllabusTrackerList !== null ? <span>Update</span> : <span>Create</span>
                                                }
                                            </Button>
                                        </div>
                                        : null
                                }
                            </div>
                        </div>
                        :
                        null
                }

            </div>

            {/* ModalChapterSection */}
            <Modal style={{ marginTop: "200px" }} className="my-modal" isOpen={editsyllabus.syllabusEditChapterName} toggle={togglesyllabusChapterModal}>
                <ModalHeader cssModule={{ 'modal-title': 'w-100 text-center' }} toggle={togglesyllabusChapterModal}>
                    <Button style={{ fontSize: "60%" }} variant="contained" color="primary">Syllabus Chapter</Button>
                </ModalHeader>
                <ModalBody>
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                        <div className="col-xs-12 col-sm-12 col-md-7 col-lg-7 text-center">
                            <TextField name="syllabusEditChapterName" value={editsyllabusField.syllabusEditChapterName} onChange={editsyllabusFieldHandler} style={{ width: "60%" }} label="Chapter Name" />
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-center">
                            <Button disabled={editsyllabusField.syllabusEditChapterName === ''} onClick={() => editSyllabusSubmit("CHAPTER")} style={{ fontSize: "90%" }} variant="contained" color="primary">Edit</Button>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button style={{ fontSize: "70%" }} variant="contained" color="secondary" onClick={togglesyllabusChapterModal}>Cancel</Button>
                </ModalFooter>
            </Modal>

            {/* ModalReferenceSection */}
            <Modal style={{ marginTop: "200px" }} className="my-modal" isOpen={editsyllabus.syllabusEditReferenceName} toggle={togglesyllabusReferenceModal}>
                <ModalHeader cssModule={{ 'modal-title': 'w-100 text-center' }} toggle={togglesyllabusReferenceModal}>
                    <Button style={{ fontSize: "60%" }} variant="contained" color="primary">Syllabus Reference</Button>
                </ModalHeader>
                <ModalBody>
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
                        <div className="col-xs-12 col-sm-12 col-md-7 col-lg-7 text-center">
                            <TextField name="syllabusEditReferenceName" value={editsyllabusField.syllabusEditReferenceName} onChange={editsyllabusFieldHandler} style={{ width: "60%" }} label="Reference Name" />
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2 text-center">
                            <Button disabled={editsyllabusField.syllabusEditReferenceName === ''} onClick={() => editSyllabusSubmit("REFERENCE")} style={{ fontSize: "90%" }} variant="contained" color="primary">Edit</Button>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button style={{ fontSize: "70%" }} variant="contained" color="secondary" onClick={togglesyllabusReferenceModal}>Cancel</Button>
                </ModalFooter>
            </Modal>

            {open === true ? <Snackbar handleClose={handleClose} status={MessageHandler.status} message={MessageHandler.message} openStatus={open} /> : null}
        </div>
    )
}

export default SyllabusTrackerDashboard




