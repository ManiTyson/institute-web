import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import ChildRoutes from '../ChildRoutes/ChildRoutes';
import { useHistory, useLocation } from "react-router-dom";
import axios from 'axios';
import { baseUrl } from '../ServerUrls';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    title: {
        flexGrow: 1,
    },
    root: {
        display: 'flex',
    },
    appBar: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        backgroundColor: '#0080ff',
        flexGrow: 1,
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
}));


function DashboardSideNav(props) {
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = React.useState(true);
    const [openList, setOpenList] = React.useState(false);
    const [loggedUser, setloggedUser] = useState(null);

    let history = useHistory();
    let location = useLocation();

    let loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');

    let roleStatus = localStorage.getItem('Role')

    console.log(roleStatus, "RoleStatus");


    useEffect(() => {
        console.log(history);
        if (loggedInStatus === false) {
            history.push(
                {
                    pathname: "/home"
                }
            )
        }
        else {
            let userName = localStorage.getItem('loggedUser');
            console.log(userName);
            setloggedUser(userName);
        }
    }, []);

    const handleClick = () => {
        setOpenList(!openList);
    };

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const myButtonHandler = () => {
        alert("Cool");
    }

    // Child Routes

    const instituteOnBoardHandler = () => {
        history.push(
            {
                pathname: '/dashboard/institute/onBoard'
            }
        )
    }
    const instituteHomeHandler = () => {
        history.push(
            {
                pathname: '/dashboard/institute/home'
            }
        )
    }
    const instituteTeacherHandler = () => {
        history.push(
            {
                pathname: '/dashboard/institute/teacher'
            }
        )
    }
    const instituteClassAnsSectionHandler = () => {
        history.push(
            {
                pathname: '/dashboard/institute/class'
            }
        )
    }
    const instituteStudentHandler = () => {
        history.push(
            {
                pathname: '/dashboard/institute/student'
            }
        )
    }
    const instituteTimetableHandler = () => {
        history.push(
            {
                pathname: '/dashboard/institute/timetable'
            }
        )
    }
    const attendanceHandler = () => {
        history.push(
            {
                pathname: '/dashboard/attendance'
            }
        )
    }
    const examHandler = () => {
        history.push(
            {
                pathname: '/dashboard/examDashboard'
            }
        )
    }
    const examScheduleHandler = () => {
        history.push(
            {
                pathname: '/dashboard/examScheduleDashboard'
            }
        )
    }
    const examMarksHandler = () => {
        history.push(
            {
                pathname: '/dashboard/examMarks'
            }
        )
    }
    const syllabusTrackerHandler = () => {
        history.push(
            {
                pathname: '/dashboard/syllabusTrackerDashboard'
            }
        )
    }
    const homeWorkHandler = () => {
        history.push(
            {
                pathname: '/dashboard/homeWorkDashboard'
            }
        )
    }
    const onlineResourceHandler = () => {
        history.push(
            {
                pathname: '/dashboard/onlineResource'
            }
        )
    }
    const configurationHandler = () => {

        axios.get(baseUrl + '/user/userInfo', { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
        .then(response => {
            console.log(response);
            if (response.data.success === true) {
                history.push(
                    {
                        pathname: '/configurations',
                        state: {
                            data: response.data.data
                        }
                    }
                );
            }
            else {

            }
        })
        .catch(error => {
            console.log(error, error.response, error.message, error.request);
        })

    }

    // Main Routes

    const instituteWebHome = () => {
        history.push(
            {
                pathname: '/home'
            }
        )
    }

    const logoutHandler = () => {
        history.push(
            {
                pathname: '/login'
            }
        );
        localStorage.removeItem('loggedUser');
        localStorage.removeItem('loggedIn');
        localStorage.removeItem('InstituteId');
        localStorage.removeItem('UserId');
        localStorage.removeItem('Role');
    }

    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, open && classes.hide)}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography className={classes.title} style={{ cursor: "pointer" }} onClick={instituteWebHome} variant="h6" noWrap>
                        <i className="fa fa-graduation-cap"></i>Institute-Web
            </Typography>
                    <Avatar style={{ width: "30px", height: "30px" }} src="/broken-image.jpg" />&nbsp;&nbsp;
                    {loggedUser}
                </Toolbar>
            </AppBar>
            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="left"
                open={open}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div className={classes.drawerHeader}>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                </div>
                <Divider />
                <List>
                    <ListItem button onClick={instituteHomeHandler}>
                        <ListItemIcon><span style={{ fontSize: "25px" }} className="material-icons">home</span></ListItemIcon>
                        <ListItemText primary="Home" />
                    </ListItem>
                    {
                        roleStatus === 'INSTITUTE-ADMIN' ?
                            <ListItem button onClick={instituteOnBoardHandler}>
                                <ListItemIcon><span style={{ fontSize: "25px" }} className="material-icons">assignment_ind</span></ListItemIcon>
                                <ListItemText primary="Onboard" />
                            </ListItem>
                            : null
                    }
                    <ListItem button onClick={instituteTeacherHandler}>
                        <ListItemIcon><i style={{ fontSize: "20px" }} className="fas fa-chalkboard-teacher"></i></ListItemIcon>
                        <ListItemText primary="ManageTeachers" />
                    </ListItem>
                    <ListItem button onClick={instituteClassAnsSectionHandler}>
                        <ListItemIcon><span style={{ fontSize: "25px" }} className="material-icons">class</span></ListItemIcon>
                        <ListItemText primary="ClassAndSection" />
                    </ListItem>
                    <ListItem button onClick={instituteStudentHandler}>
                        <ListItemIcon><i style={{ fontSize: "20px" }} className="fas fa-user-graduate"></i></ListItemIcon>
                        <ListItemText primary="ManageStudents" />
                    </ListItem>
                    <ListItem button onClick={instituteTimetableHandler}>
                        <ListItemIcon><i style={{ fontSize: "20px" }} className="fa fa-calendar"></i></ListItemIcon>
                        <ListItemText primary="Timetable" />
                    </ListItem>
                    {
                        roleStatus !== 'STUDENT' ?
                            <ListItem button onClick={attendanceHandler}>
                                <ListItemIcon><i style={{ fontSize: "20px" }} className="fa fa-clock-o"></i></ListItemIcon>
                                <ListItemText primary="Attendance" />
                            </ListItem>
                            : null
                    }
                    <ListItem button onClick={handleClick}>
                        <ListItemIcon>
                            <i style={{ fontSize: "20px" }} className="fa fa-file-text"></i>
                        </ListItemIcon>
                        <ListItemText primary="Exam" />
                        {openList ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={openList} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding>
                            <ListItem button onClick={examHandler} className={classes.nested}>
                                <ListItemIcon>
                                    <i style={{ fontSize: "20px" }} className="fa fa-calendar-plus-o"></i>
                                </ListItemIcon>
                                <ListItemText primary="Add/Edit Exam" />
                            </ListItem>
                            <ListItem button onClick={examScheduleHandler} className={classes.nested}>
                                <ListItemIcon>
                                    <i style={{ fontSize: "20px" }} className="fa fa-pencil-square-o"></i>
                                </ListItemIcon>
                                <ListItemText primary="Exam Schedule" />
                            </ListItem>
                        </List>
                    </Collapse>
                    {
                        roleStatus !== 'STUDENT' ?
                            <ListItem button onClick={examMarksHandler}>
                                <ListItemIcon><i style={{ fontSize: "20px" }} className="fa fa-hourglass-start"></i></ListItemIcon>
                                <ListItemText primary="ExamMarks" />
                            </ListItem>
                            : null
                    }
                    <ListItem button onClick={syllabusTrackerHandler}>
                        <ListItemIcon><i style={{ fontSize: "20px" }} className="fa fa-book"></i></ListItemIcon>
                        <ListItemText primary="SyllabusTracker" />
                    </ListItem>
                    {
                        roleStatus !== 'STUDENT' ?
                            <ListItem button onClick={homeWorkHandler}>
                                <ListItemIcon><i style={{ fontSize: "20px" }} className="fa fa-tasks"></i></ListItemIcon>
                                <ListItemText primary="HomeWork" />
                            </ListItem>
                            : null
                    }
                    {
                        roleStatus !== 'TEACHER' ?
                            <ListItem button onClick={onlineResourceHandler}>
                                <ListItemIcon><i style={{ fontSize: "20px" }} className="fa fa-window-restore"></i></ListItemIcon>
                                <ListItemText primary="Online Resource" />
                            </ListItem>
                            : null
                    }
                    <ListItem button onClick={configurationHandler}>
                        <ListItemIcon><i style={{ fontSize: "20px" }} className="fa fa-cogs"></i></ListItemIcon>
                        <ListItemText primary="Configurations" />
                    </ListItem>
                    <ListItem button onClick={logoutHandler}>
                        <ListItemIcon><i style={{ fontSize: "25px" }} className="fas fa-sign-out-alt" ></i></ListItemIcon>
                        <ListItemText primary="Logout" />
                    </ListItem>
                </List>
                <Divider />
            </Drawer>
            <main
                className={clsx(classes.content, {
                    [classes.contentShift]: open,
                })}
            >
                <div className={classes.drawerHeader} />
                <ChildRoutes>
                    {props.children}
                </ChildRoutes>
            </main>
        </div>
    );
}

export default DashboardSideNav

