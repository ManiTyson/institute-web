import React, { useState, useEffect } from 'react';
import "./SignUp.css";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';

function SignUp(props) {

    const [signUp, setsignUp] = useState({
        firstName: '',
        lastName: '',
        emailId: '',
        mobileNumber: '',
        DOB: new Date(),
        role: ''
    });
    const [allRoles, setallRoles] = useState([]);

    const roleData = [
        {
            id: 1,
            label: 'INSTITUTE-ADMIN'
        },
        {
            id: 2,
            label: 'STUDENT'
        },
        {
            id: 3,
            label: 'TEACHER'
        },
        {
            id: 4,
            label: 'GUEST'
        }
    ];

    const getSignedUp = (e) => {
        setsignUp({ ...signUp, [e.target.name]: e.target.value });
        console.log(signUp, signUp.DOB.getFullYear() + "-" + (signUp.DOB.getMonth() + 1) + "-" + signUp.DOB.getDate());
    }

    useEffect(() => {
        setallRoles(roleData);
    }, []);

    return (
        <div>
            <div className="container-fluid my-container">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4"></div>
                    <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-center">
                        <br /><br />
                        <div className="login">
                            <h5 style={{ color: "#B99E01" }}><i className="fa fa-graduation-cap"></i>Institute-<span style={{ color: "black" }}>Web</span></h5>
                            <h6 style={{ color: "#B99E01" }}>Sign<span style={{ color: "black" }}>Up</span></h6><br />
                            <form>
                                <input placeholder="FirstName" type="text" name="firstName" value={signUp.firstName} onChange={getSignedUp} />
                                <br /><div style={{ marginTop: "2px" }} />
                                <input placeholder="LastName" type="text" name="lastName" value={signUp.lastName} onChange={getSignedUp} />
                                <br /><div style={{ marginTop: "2px" }} />
                                <input name="emailId" placeholder="EmailId" type="email" value={signUp.emailId} onChange={getSignedUp} />
                                <br /><div style={{ marginTop: "2px" }} />
                                <input name="mobileNumber" placeholder="MobileNumber" type="number" value={signUp.mobileNumber} onChange={getSignedUp} />
                                <br /><div style={{ marginTop: "2px" }} />
                                <div className="customDatePickerWidth">
                                    <DatePicker dateFormat="yyyy/MM/dd" showMonthDropdown showYearDropdown selected={signUp.DOB} onChange={date => setsignUp({ ...signUp, DOB: date })} /></div>
                                <FormControl style={{ width: "100%" }}>
                                    <InputLabel id="demo-simple-select-label">Roles</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={signUp.role}
                                        onChange={getSignedUp}
                                        inputProps={{
                                            name: "role"
                                        }}
                                    // name="instituteType"
                                    >
                                        {allRoles.map((option, index) => (
                                            <MenuItem key={option.id} value={option.label}>
                                                {option.label}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl><br /><br />
                                <button onClick={() => props.mySignUpHandler(
                                    {
                                        "firstName": signUp.firstName,
                                        "lastName": signUp.lastName,
                                        "emailId": signUp.emailId,
                                        "mobileNumber": signUp.mobileNumber,
                                        "dateOfBirth": signUp.DOB.getFullYear() + "-" + (signUp.DOB.getMonth() + 1) + "-" + signUp.DOB.getDate(),
                                        "role": signUp.role, 
                                        "otp":'', 
                                        "type":"SIGNUP"
                                    }
                                )} className="button button1" type="button"
                                    disabled={
                                        signUp.firstName === '' ||
                                        signUp.lastName === '' ||
                                        signUp.emailId.indexOf("@") === -1 ||
                                        signUp.mobileNumber === '' ||
                                        signUp.role === '' ||
                                        signUp.mobileNumber.length !== 10
                                    }>SignUp</button><br /><br />
                                <span>
                                    <p style={{ fontSize: "80%", color: "#B99E01" }}>Institute-Web v1.0<br />
                                        <span style={{ color: "black" }}>Institute-Web Limited© 2020</span></p>
                                </span>
                            </form>
                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4"></div>
                </div>
            </div>
        </div>
    )
}

export default SignUp