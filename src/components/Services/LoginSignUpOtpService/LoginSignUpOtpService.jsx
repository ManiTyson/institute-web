import React, { useState } from 'react';
import axios from 'axios';
import { baseUrl } from '../../ServerUrls';
import { Modal, ModalHeader, ModalFooter, ModalBody } from 'reactstrap';
import { useHistory } from "react-router-dom";
import Button from '@material-ui/core/Button';
import PersonIcon from '@material-ui/icons/Person';
import "./LoginSignUpOtpService.css";
import Snackbar from '../../SnackBar/SnackBar';

function LoginSignUpOtpService(props) {

    let history = useHistory();

    const [MessageHandler, setMessageHandler] = useState({ message: '', status: true });

    const [open, setOpen] = React.useState(false);

    const [isRoleModalOpen, setisRoleModalOpen] = useState(false);
    const [isOtpModalOpen, setisOtpModalOpen] = useState(false);

    const [tempData, settempData] = useState({});

    const toggleRoleModal = () => {
        setisRoleModalOpen(!isRoleModalOpen);
    }
    const toggleOtpModal = () => {
        setisOtpModalOpen(!isOtpModalOpen);
    }

    // const myRouteHandler = () => {
    //     localStorage.setItem('loggedIn', 'true');
    //     localStorage.setItem('loggedUser', tempData.mobileNumber);
    //     history.push(
    //         {
    //             pathname: '/dashboard',
    //             state: {
    //                 loggedUser: tempData.mobileNumber
    //             }
    //         }
    //     );
    // }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const myLoginHandler = (loginDetails) => {

        console.log("LoginDetails", loginDetails,tempData);
        axios.post(baseUrl + '/user/otp/verify', loginDetails, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==' } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: true });
                    if(response.data.data.instituteId !== null)
                    localStorage.setItem('UserId', response.data.data.instituteUser.id);
                    checkConfigurations();
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                }
                handleClick();
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                handleClick();
            })
    }

    const checkConfigurations = () => {

        axios.get(baseUrl + '/user/userInfo', { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==', 'userId': localStorage.getItem('UserId') } })
        .then(response => {
            console.log(response);
            if (response.data.success === true) {
                localStorage.setItem('loggedIn', 'true');
                localStorage.setItem('loggedUser', tempData.mobileNumber);
                history.push(
                    {
                        pathname: '/configurations',
                        state: {
                            data: response.data.data
                        }
                    }
                );
            }
            else {
                setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                handleClick();
            }
        })
        .catch(error => {
            console.log(error, error.response, error.message, error.request);
            setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
            handleClick();
        })

    }

    const mySignUpHandler = (signUpDetails) => {

        console.log("SignUpDetails", signUpDetails);
        settempData({});
        axios.post(baseUrl + '/user/signup', signUpDetails, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==' } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: true });
                    settempData(signUpDetails);
                    toggleOtpModal();
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    settempData({});
                }
                handleClick();
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                settempData({});
                handleClick();
            })
    }

    const getOtpHandler = (otpDetails) => {

        console.log("OtpDetails", otpDetails);
        settempData({});
        axios.post(baseUrl + '/user/login', otpDetails, { headers: { 'Authorization': 'Basic bmFyYXNpbW1hbjoxMjM0NTY4OQ==' } })
            .then(response => {
                console.log(response);
                if (response.data.success === true) {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: true });
                    settempData(otpDetails);
                }
                else {
                    setMessageHandler({ ...MessageHandler, message: response.data.message, status: false });
                    settempData({});
                }
                handleClick();
            })
            .catch(error => {
                console.log(error, error.response, error.message, error.request);
                setMessageHandler({ ...MessageHandler, message: error.response.data.message, status: false });
                settempData({});
                handleClick();
            })
    }

    return (
        <div>
        
            {open === true ? <Snackbar handleClose = {handleClose} status = {MessageHandler.status} message = {MessageHandler.message} openStatus = {open} /> : null}

            {/* ModalRole */}
            {/* <Modal isOpen={isRoleModalOpen} toggle={toggleRoleModal}>
                <ModalHeader cssModule={{ 'modal-title': 'w-100 text-center' }} toggle={toggleRoleModal}>
                    <Button style={{ fontSize: "60%" }} variant="contained" color="primary">Roles</Button>
                </ModalHeader>
                <ModalBody>
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
                        <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center">
                            <Button onClick={myRouteHandler} startIcon={<i className="fa fa-graduation-cap"></i>} style={{ fontSize: "90%" }} variant="contained" color="primary">Institute</Button>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
                        <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center">
                            <Button onClick={myRouteHandler} startIcon={<i className="fas fa-chalkboard-teacher"></i>} style={{ fontSize: "90%" }} variant="contained" color="primary">Teacher</Button>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
                    </div><br /><br />
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
                        <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center">
                            <Button onClick={myRouteHandler} startIcon={<i className="fas fa-user-graduate"></i>} style={{ fontSize: "90%" }} variant="contained" color="primary">Student</Button>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
                        <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center">
                            <Button onClick={myRouteHandler} startIcon={<PersonIcon />} style={{ fontSize: "90%" }} variant="contained" color="primary">Guest</Button>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button style={{ fontSize: "70%" }} variant="contained" color="secondary" onClick={toggleRoleModal}>Cancel</Button>
                </ModalFooter>
            </Modal> */}

            {/* ModalOtp */}
            <Modal className="my-modal" isOpen={isOtpModalOpen} toggle={toggleOtpModal}>
                <ModalHeader cssModule={{ 'modal-title': 'w-100 text-center' }} toggle={toggleOtpModal}>
                    <Button style={{ fontSize: "60%" }} variant="contained" color="primary">OTP</Button>
                </ModalHeader>
                <ModalBody>
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
                        <div className="col-xs-12 col-sm-12 col-md-5 col-lg-5 text-center">
                                <input placeholder="OTP" type="number" value={tempData.otp} onChange={e => {
                                    settempData({ ...tempData, otp: e.target.value })
                                }} />
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center">
                            <Button disabled={tempData.otp === ''} onClick={() => myLoginHandler(tempData)} style={{ fontSize: "90%" }} variant="contained" color="primary">Submit</Button>
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button style={{ fontSize: "70%" }} variant="contained" color="secondary" onClick={toggleOtpModal}>Cancel</Button>
                </ModalFooter>
            </Modal>
            {props.children(myLoginHandler, getOtpHandler, mySignUpHandler)}
        </div>
    )
}

export default LoginSignUpOtpService
